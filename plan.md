# Plan for RedInk, codename "`red`"

Minimalistic bitmap image editor aimed for developers and casual users.

## Fundementals and Performance
- Keep it simple; prefer prebuilt solutions.
- Always start under <1 second; extensions are loaded only when needed.
- Fast when performing simple tasks.
- Not allowed to crash; Always prefer displaying error and safe way to restore workspace.
- Binary size should be minimal! Use demo scene tools to pack it further.
- No limit on memory usage, we are allowed to waste while keeping performance.
- Only support image sizes up to max texture size that hardware support.

## Features
- Subset of Photoshop 4 features.
- Aim for bitmap, but have vector tools.

## Usage
- _Everything_ is a command. Examples: `layer.copy`, `layer.move`, `core.quit`, `tool.set com.redink.tools.selection`, `file.save` etc.
- Should be able to find & run every command/tool through the lookup menu, similar as Blender and Sublime Text. Could be as simple as a Lua prompt.
- Only one workspace active at a time, panels have this as context.

## GUI
- Windowing and panels should work similar to Blender.
- Should be able to break out one or more panels into separate window.
- Skinnable through extensions.

## Extendability
- Import and export: Lua script that has import() and export() functions.
- Filters: Lua script that has apply() and possibility to use GLSL shaders.
- Extensions can register panels, so we need to expose some sort of GUI binding.
- Themes: GUI rendering specified in Lua scripts.
- All tools, importers/exporters and filters are built as extenions, unless performance critical.

## History
- Version the file? If possible through SQLite, otherwise save interations into temp workspace folder.
- Alternative solution: use GIT??? Or similar versioning backend.

## Rendering & Compositing
- Render layers bottom up; keep cache at each layer.
- Use hash of previous layers for invalidation; redraw only if layer type depends on previous layers.
- Tools can render usage/tool overlays. (Examples; selection rect, magnification glas.) This is run in the GUI thread.

## Responsiveness
- Three separate threads; GUI, composite and operations.
    - GUI: renders in main thread and handles input.
    - Composite: renders composite, layers and previews.
    - Operations: runs heavy operations, tools, import/export etc.
- Operations should always be possible to cancel.

## Testing and CI
- Use testing framework, such as gtest.
- Each build should pass some sort of CI solution to know that nothing breaks.

## Libraries
- Logging: https://github.com/emilk/loguru
- Profiling: https://github.com/jonasmr/microprofile or https://github.com/Celtoys/Remotery

## File format
Just as described in the [The Shape of Everything](http://shapeof.com/archives/2013/4/we_need_a_standard_layered_image_format.html) blog post, and how the Acorn format works; use a SQLite database for internal format.

Possibility to save incremental updates.

### Example

    redink file format; .red
    +---------------------------------------------------+
    | SQLite Database                                   |
    |                                                   |
    | Tables:                                           |
    |  +--------------------------------------------+   |
    |  | image_attributes                           |   |
    |  +--------------------------------------------+   |
    |                                                   |
    |  +--------------------------------------------+   |
    |  | layers                                     |   |
    |  +--------------------------------------------+   |
    |                                                   |
    |  +--------------------------------------------+   |
    |  | layer_attributes                           |   |
    |  +--------------------------------------------+   |
    |                                                   |
    +---------------------------------------------------+

### Questions
    Q: Should RI format be compatible with Acorn?
    A: Initially, no. But use same tables, so the possibility for compatibilty is open down the line.

    Q: Should app always perform alterations directly to disk?
    A: No, but there could be an option for it. Otherwise a copy is kept in memory and synced to disk on save. Sync is essentially only writing a list of modifications to the database. Turning the option on would mean that each operation is synced.

    Q: What about size on disk?
    A: Initially don't care, it's just a workspace format. In the future, could have possibility to automatically compress it using LZ4 or similar.

## Versioning
Format: [major].[minor].[fixes]
    [major] = Breaking API, functionality or format changes.
    [minor] = Can change API with deprecation. No format changes.
    [fixes] = Only bug fixes.

### Updates
Look into prebuilt crossplatform solutions, don't roll our own.

### Versioning names
Every major and minor update should be named. Use Photoshop nostalgia, graphical engineering and Gothenburg familiar names, start with Knoll brothers.
`3.1.0 - Knoll Edition`
`3.2.0 - Thomas Edition`
`3.3.0 - John Edition`
`3.4.0 - Shop Edition`
`3.5.0 - Cornell Edition`
`3.6.0 - Blinn Edition`
`3.7.0 - Vera Edition`
`3.8.0 - Malin Edition`
`3.9.0 - Marita Edition`
`3.10.0 - Torbjorn Edition`
`3.11.0 - Hisingen Edition`
`3.12.0 - Kyrkbytorget Edition`
`3.13.0 - Gamlestan Edition`

### Easter Eggs
Always keep in theme with old Photoshop About/splash easter eggs.
- Games; asteroids, arkanoid, doom
- Demoscene effects
