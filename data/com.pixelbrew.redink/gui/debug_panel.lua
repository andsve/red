-- print(__panel_pointer)
panel.register("core.panel.debug")

math.randomseed(1337)

function init(self)
    self.r = math.random()
    self.g = math.random()
    self.b = math.random()
    print("debug panel initiated: " .. tostring(self) .. string.format(" (%f %f %f)", self.r, self.g, self.b))
end

function draw(self, x0, y0, x1, y1)

    gui.layout_row_dynamic(0, 2)
    gui.label("Apa!", gui.TEXT_LEFT, {1,0,0,1})
    -- print("in debug panel draw: " .. tostring(self) .. string.format("[%d, %d, %d, %d (w: %d h: %d)", x0, y0, x1, y1, x1-x0, y1-y0 ))
    -- print(string.format("%f %f %f", self.r, self.g, self.b))
    -- gfx.clear_color(self.r,self.g,self.b,0)
    -- gfx.clear()
end
