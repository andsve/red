#ifndef RED_CORE_GUI_PRIVATE_H
#define RED_CORE_GUI_PRIVATE_H

#include "nuklear.h"

namespace riGui
{

    struct State
    {
        // nuklear context
        nk_context m_Ctx;

        // Window and buffer sizes
        int m_WindowWidth;
        int m_WindowHeight;
        int m_DisplayWidth;
        int m_DisplayHeight;
        float m_FBScaleX;
        float m_FBScaleY;

        // Input
        unsigned int m_Text[256];
        int m_TextLen;
        struct nk_vec2 m_Scroll;
        double m_LastButtonClick;
    };
}

#endif // RED_CORE_GUI_PRIVATE_H
