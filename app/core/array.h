#ifndef CORE_ARRAY_H
#define CORE_ARRAY_H

#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

template <typename T>
class riArray
{
private:
    uint64_t m_Capacity;
    uint64_t m_Size;
    T*       m_Data;

public:
    riArray();
    riArray(uint64_t capacity, uint64_t size);
    ~riArray();

    uint64_t Size();
    uint64_t Capacity();
    uint64_t Remains();
    void SetSize(uint64_t size);
    void SetCapacity(uint64_t capacity);
    T& First();
    T& Last();
    T& operator[](uint64_t i);
    const T& operator[](uint64_t i) const;
    uint64_t Put(T entry);
    void Remove(T entry);
    void RemoveAll(T entry);
    void RemoveIndex(uint64_t i);
};


template <typename T>
riArray<T>::riArray()
{
    m_Capacity = 0;
    m_Size = 0;
    m_Data = 0x0;
}

template <typename T>
riArray<T>::riArray(uint64_t capacity, uint64_t size)
{
    m_Capacity = capacity;
    m_Size = size;
    m_Data = (T*)malloc(capacity * sizeof(T));
}

template <typename T>
riArray<T>::~riArray()
{
    if (m_Data) {
        delete m_Data;
    }
}

template <typename T>
uint64_t riArray<T>::Put(T entry)
{
    assert(m_Size + 1 <= m_Capacity);
    m_Data[m_Size++] = entry;
    return (m_Size-1);
}

template <typename T>
void riArray<T>::RemoveIndex(uint64_t i)
{
    assert(i < m_Size);
    for (; i < m_Size-1; ++i)
    {
        m_Data[i] = m_Data[i+1];
    }
    m_Size--;
}

template <typename T>
void riArray<T>::Remove(T entry)
{
    assert(m_Size > 0);
    uint64_t i = 0;

    // Find entry
    for (; i < m_Size; ++i)
    {
        if (m_Data[i] == entry)
        {
            break;
        }
    }

    for (; i < m_Size-1; ++i)
    {
        m_Data[i] = m_Data[i+1];
    }
    m_Size--;
}

template <typename T>
void riArray<T>::RemoveAll(T entry)
{
    assert(m_Size > 0);
    uint64_t i = 0;
    uint64_t j = 0;

    // Find entry
    for (; j < m_Size; ++j, ++j)
    {
        printf("%llu %llu\n", j, i);
        if (m_Data[j] == entry)
        {
            j++;
        }
        m_Data[i] = m_Data[j];
    }

    m_Size-=j-i;
}

template <typename T>
uint64_t riArray<T>::Size()
{
    return m_Size;
}

template <typename T>
uint64_t riArray<T>::Capacity()
{
    return m_Capacity;
}

template <typename T>
uint64_t riArray<T>::Remains()
{
    return m_Capacity - m_Size;
}

template <typename T>
void riArray<T>::SetSize(uint64_t size)
{
    assert(size <= m_Capacity);
    m_Size = size;
}

template <typename T>
void riArray<T>::SetCapacity(uint64_t capacity)
{
    if (capacity == 0) {
        delete m_Data;
        m_Data = 0x0;
        m_Capacity = 0;
        m_Size = 0;
        return;
    }

    T* new_data = (T*)malloc(capacity * sizeof(T));

    // no need to copy data
    if (m_Data == 0x0)
    {
        m_Capacity = capacity;
        m_Data = new_data;
        return;
    }

    uint64_t new_size = m_Size > capacity ? capacity : m_Size;
    memcpy(new_data, m_Data, new_size * sizeof(T));

    m_Size = new_size;
    m_Capacity = capacity;
    m_Data = new_data;
}

template <typename T>
T& riArray<T>::First()
{
    assert(Size() > 0);
    return m_Data[0];
}

template <typename T>
T& riArray<T>::Last()
{
    assert(Size() > 0);
    return m_Data[Size()-1];
}

template <typename T>
T& riArray<T>::operator[](uint64_t i)
{
    assert(i <= Size());
    return m_Data[i];
}

template <typename T>
const T& riArray<T>::operator[](uint64_t i) const
{
    assert(i <= Size());
    return m_Data[i];
}



#endif
