#include "resource.h"

#include <physfs.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>

using namespace riRes;

static bool g_ResInitiated = false;

Result riRes::Init(const char* argv0)
{
    if (g_ResInitiated)
    {
        LOG_F(WARNING, "Resource system already initiated.");
        return RESULT_OK;
    }

    if ( !PHYSFS_init( argv0 ) )
    {
        LOG_F(ERROR, "PHYSFS_init failed: %s", PHYSFS_getLastError());
        return RESULT_ERROR;
    }

    // mount physfs directories
    if ( !PHYSFS_setSaneConfig( "pixelbrew", "redink", "rip", 0, 0 ) )
    {
        LOG_F(ERROR, "PhysFS_setSaneConfig failed: %s", PHYSFS_getLastError());
    }
    PHYSFS_mount( "./", 0x0, 0 );
    PHYSFS_mount( "./data/", 0x0, 0 );
    PHYSFS_mount( argv0, 0x0, 0 );

    g_ResInitiated = true;

    return RESULT_OK;
}

void riRes::Terminate()
{
    // g_ResInitiated = false;
}

Result riRes::Get(const char* path, char** data, uint64_t& size, bool auto_terminate)
{

    PHYSFS_File *file = PHYSFS_openRead( path );
    if (!file)
    {
        LOG_F(ERROR, "Could not open file: %s, error: %s", path, PHYSFS_getLastError() );
        return RESULT_ERROR;
    }

    size = PHYSFS_fileLength(file);
    // if (auto_terminate) {
        // size += 1;
    // }
    *data = (char*)malloc(size + (auto_terminate ? 1 : 0));

    uint64_t data_read = 0;
    while (size > data_read) {
        data_read += PHYSFS_readBytes(file, *data + data_read, size - data_read);
    }
    PHYSFS_close(file);

    if (auto_terminate)
    {
        (*data)[size] = '\0';
    }

    return RESULT_OK;
}
