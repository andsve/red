#include "GL/glew.h"

#include "core.h"
#include "gui.h"
#include "script_private.h"
#include "window_private.h"

#define NK_INCLUDE_FIXED_TYPES
#define NK_INCLUDE_STANDARD_IO
#define NK_INCLUDE_STANDARD_VARARGS
#define NK_INCLUDE_DEFAULT_ALLOCATOR
#define NK_INCLUDE_VERTEX_BUFFER_OUTPUT
#define NK_INCLUDE_FONT_BAKING
#define NK_INCLUDE_DEFAULT_FONT
#define NK_IMPLEMENTATION
#include "gui_private.h" // "nuklear.h" is included here

using namespace riGui;

struct RenderCtx
{
    // Font
    struct nk_font_atlas m_Atlas;
    GLuint m_FontTex;
    nk_draw_null_texture m_NullTex;

    // nuklear command buffer
    struct nk_buffer m_Cmds;

    // OGL stuff
    GLuint m_VBO, m_VAO, m_EBO;
    GLuint m_Prog;
    GLuint m_VertShdr;
    GLuint m_FragShdr;
    GLint m_AttribPos;
    GLint m_AttribUv;
    GLint m_AttribCol;
    GLint m_UniformTex;
    GLint m_UniformProj;
};

struct nk_gui_vertex {
    float position[2];
    float uv[2];
    nk_byte col[4];
};

static RenderCtx g_RenderCtx;

static void _InitGL()
{
    GLint status;
    static const GLchar *vertex_shader =
        "#version 150\n"
        "uniform mat4 ProjMtx;\n"
        "in vec2 Position;\n"
        "in vec2 TexCoord;\n"
        "in vec4 Color;\n"
        "out vec2 Frag_UV;\n"
        "out vec4 Frag_Color;\n"
        "void main() {\n"
        "   Frag_UV = TexCoord;\n"
        "   Frag_Color = Color;\n"
        "   gl_Position = ProjMtx * vec4(Position.xy, 0, 1);\n"
        "}\n";
    static const GLchar *fragment_shader =
        "#version 150\n"
        "precision mediump float;\n"
        "uniform sampler2D Texture;\n"
        "in vec2 Frag_UV;\n"
        "in vec4 Frag_Color;\n"
        "out vec4 Out_Color;\n"
        "void main(){\n"
        "   Out_Color = Frag_Color * texture(Texture, Frag_UV.st);\n"
        "}\n";

    // struct nk_glfw_device *dev = &glfw.ogl;
    nk_buffer_init_default(&g_RenderCtx.m_Cmds);
    g_RenderCtx.m_Prog = glCreateProgram();
    g_RenderCtx.m_VertShdr = glCreateShader(GL_VERTEX_SHADER);
    g_RenderCtx.m_FragShdr = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(g_RenderCtx.m_VertShdr, 1, &vertex_shader, 0);
    glShaderSource(g_RenderCtx.m_FragShdr, 1, &fragment_shader, 0);
    glCompileShader(g_RenderCtx.m_VertShdr);
    glCompileShader(g_RenderCtx.m_FragShdr);
    glGetShaderiv(g_RenderCtx.m_VertShdr, GL_COMPILE_STATUS, &status);
    assert(status == GL_TRUE);
    glGetShaderiv(g_RenderCtx.m_FragShdr, GL_COMPILE_STATUS, &status);
    assert(status == GL_TRUE);
    glAttachShader(g_RenderCtx.m_Prog, g_RenderCtx.m_VertShdr);
    glAttachShader(g_RenderCtx.m_Prog, g_RenderCtx.m_FragShdr);
    glLinkProgram(g_RenderCtx.m_Prog);
    glGetProgramiv(g_RenderCtx.m_Prog, GL_LINK_STATUS, &status);
    assert(status == GL_TRUE);

    g_RenderCtx.m_UniformTex = glGetUniformLocation(g_RenderCtx.m_Prog, "Texture");
    g_RenderCtx.m_UniformProj = glGetUniformLocation(g_RenderCtx.m_Prog, "ProjMtx");
    g_RenderCtx.m_AttribPos = glGetAttribLocation(g_RenderCtx.m_Prog, "Position");
    g_RenderCtx.m_AttribUv = glGetAttribLocation(g_RenderCtx.m_Prog, "TexCoord");
    g_RenderCtx.m_AttribCol = glGetAttribLocation(g_RenderCtx.m_Prog, "Color");

    {
        /* buffer setup */
        GLsizei vs = sizeof(struct nk_gui_vertex);
        size_t vp = offsetof(struct nk_gui_vertex, position);
        size_t vt = offsetof(struct nk_gui_vertex, uv);
        size_t vc = offsetof(struct nk_gui_vertex, col);

        glGenBuffers(1, &g_RenderCtx.m_VBO);
        glGenBuffers(1, &g_RenderCtx.m_EBO);
        glGenVertexArrays(1, &g_RenderCtx.m_VAO);

        glBindVertexArray(g_RenderCtx.m_VAO);
        glBindBuffer(GL_ARRAY_BUFFER, g_RenderCtx.m_VBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_RenderCtx.m_EBO);

        glEnableVertexAttribArray((GLuint)g_RenderCtx.m_AttribPos);
        glEnableVertexAttribArray((GLuint)g_RenderCtx.m_AttribUv);
        glEnableVertexAttribArray((GLuint)g_RenderCtx.m_AttribCol);

        glVertexAttribPointer((GLuint)g_RenderCtx.m_AttribPos, 2, GL_FLOAT, GL_FALSE, vs, (void*)vp);
        glVertexAttribPointer((GLuint)g_RenderCtx.m_AttribUv, 2, GL_FLOAT, GL_FALSE, vs, (void*)vt);
        glVertexAttribPointer((GLuint)g_RenderCtx.m_AttribCol, 4, GL_UNSIGNED_BYTE, GL_TRUE, vs, (void*)vc);
    }

    glBindTexture(GL_TEXTURE_2D, 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

static void _GuiRender(HState state, nk_context* ctx, enum nk_anti_aliasing AA, int max_vertex_buffer, int max_element_buffer)
{
    GLfloat ortho[4][4] = {
        {2.0f, 0.0f, 0.0f, 0.0f},
        {0.0f,-2.0f, 0.0f, 0.0f},
        {0.0f, 0.0f,-1.0f, 0.0f},
        {-1.0f,1.0f, 0.0f, 1.0f},
    };
    ortho[0][0] /= (GLfloat)state->m_WindowWidth;
    ortho[1][1] /= (GLfloat)state->m_WindowHeight;

    ortho[3][0] += -1.0f / (GLfloat)state->m_WindowWidth;
    ortho[3][1] +=  1.0f / (GLfloat)state->m_WindowHeight;

    /* setup global state */
    glEnable(GL_BLEND);
    glBlendEquation(GL_FUNC_ADD);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_SCISSOR_TEST);
    glActiveTexture(GL_TEXTURE0);

    /* setup program */
    glUseProgram(g_RenderCtx.m_Prog);
    glUniform1i(g_RenderCtx.m_UniformTex, 0);
    glUniformMatrix4fv(g_RenderCtx.m_UniformProj, 1, GL_FALSE, &ortho[0][0]);
    glViewport(0,0,(GLsizei)state->m_DisplayWidth,(GLsizei)state->m_DisplayHeight);
    {
        /* convert from command queue into draw list and draw to screen */
        const struct nk_draw_command *cmd;
        void *vertices, *elements;
        const nk_draw_index *offset = NULL;

        /* allocate vertex and element buffer */
        glBindVertexArray(g_RenderCtx.m_VAO);
        glBindBuffer(GL_ARRAY_BUFFER, g_RenderCtx.m_VBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_RenderCtx.m_EBO);

        glBufferData(GL_ARRAY_BUFFER, max_vertex_buffer, NULL, GL_STREAM_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, max_element_buffer, NULL, GL_STREAM_DRAW);

        /* load draw vertices & elements directly into vertex + element buffer */
        vertices = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
        elements = glMapBuffer(GL_ELEMENT_ARRAY_BUFFER, GL_WRITE_ONLY);
        {
            /* fill convert configuration */
            struct nk_convert_config config;
            static const struct nk_draw_vertex_layout_element vertex_layout[] = {
                {NK_VERTEX_POSITION, NK_FORMAT_FLOAT, NK_OFFSETOF(struct nk_gui_vertex, position)},
                {NK_VERTEX_TEXCOORD, NK_FORMAT_FLOAT, NK_OFFSETOF(struct nk_gui_vertex, uv)},
                {NK_VERTEX_COLOR, NK_FORMAT_R8G8B8A8, NK_OFFSETOF(struct nk_gui_vertex, col)},
                {NK_VERTEX_LAYOUT_END}
            };
            NK_MEMSET(&config, 0, sizeof(config));
            config.vertex_layout = vertex_layout;
            config.vertex_size = sizeof(struct nk_gui_vertex);
            config.vertex_alignment = NK_ALIGNOF(struct nk_gui_vertex);
            config.null = g_RenderCtx.m_NullTex;
            config.circle_segment_count = 22;
            config.curve_segment_count = 22;
            config.arc_segment_count = 22;
            config.global_alpha = 1.0f;
            config.shape_AA = AA;
            config.line_AA = AA;

            /* setup buffers to load vertices and elements */
            {struct nk_buffer vbuf, ebuf;
            nk_buffer_init_fixed(&vbuf, vertices, (size_t)max_vertex_buffer);
            nk_buffer_init_fixed(&ebuf, elements, (size_t)max_element_buffer);
            nk_convert(ctx, &g_RenderCtx.m_Cmds, &vbuf, &ebuf, &config);}
        }
        glUnmapBuffer(GL_ARRAY_BUFFER);
        glUnmapBuffer(GL_ELEMENT_ARRAY_BUFFER);

        /* iterate over and execute each draw command */
        nk_draw_foreach(cmd, ctx, &g_RenderCtx.m_Cmds)
        {
            if (!cmd->elem_count) continue;
            glBindTexture(GL_TEXTURE_2D, (GLuint)cmd->texture.id);
            glScissor(
                (GLint)(cmd->clip_rect.x * state->m_FBScaleX),
                (GLint)((state->m_WindowHeight - (GLint)(cmd->clip_rect.y + cmd->clip_rect.h)) * state->m_FBScaleY),
                (GLint)(cmd->clip_rect.w * state->m_FBScaleX),
                (GLint)(cmd->clip_rect.h * state->m_FBScaleY));
            glDrawElements(GL_TRIANGLES, (GLsizei)cmd->elem_count, GL_UNSIGNED_SHORT, offset);
            offset += cmd->elem_count;
        }
        nk_clear(ctx);
    }

    /* default OpenGL state */
    glUseProgram(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
    glDisable(GL_BLEND);
    glDisable(GL_SCISSOR_TEST);
}

void riGui::Init()
{
    // Setup OGL rendering stuff
    _InitGL();

    // nk_init_default(&g_NKCtx, 0);
    nk_font_atlas_init_default(&g_RenderCtx.m_Atlas);
    nk_font_atlas_begin(&g_RenderCtx.m_Atlas);

    const void *image; int w, h;
    image = nk_font_atlas_bake(&g_RenderCtx.m_Atlas, &w, &h, NK_FONT_ATLAS_RGBA32);
    glGenTextures(1, &g_RenderCtx.m_FontTex);
    glBindTexture(GL_TEXTURE_2D, g_RenderCtx.m_FontTex);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei)w, (GLsizei)h, 0,
                GL_RGBA, GL_UNSIGNED_BYTE, image);
    LOG_F(INFO, "font texture: %ux%u", w, h);
    nk_font_atlas_end(&g_RenderCtx.m_Atlas, nk_handle_id((int)g_RenderCtx.m_FontTex), &g_RenderCtx.m_NullTex);

    // if (g_RenderCtx.m_Atlas.default_font)
        // nk_style_set_font(&glfw.ctx, &g_RenderCtx.m_Atlas.default_font->handle);


}

static void _glfw_clipbard_paste(nk_handle usr, struct nk_text_edit *edit)
{
    riWindow::HWindow window = (riWindow::HWindow)usr.ptr;
    const char *text = glfwGetClipboardString(window->m_GLFWwindow);
    if (text)
        nk_textedit_paste(edit, text, nk_strlen(text));
}

static void _glfw_clipbard_copy(nk_handle usr, const char *text, int len)
{
    riWindow::HWindow window = (riWindow::HWindow)usr.ptr;
    char *str = 0;
    if (!len)
        return;
    str = (char*)malloc((size_t)len+1);
    if (!str)
        return;
    memcpy(str, text, (size_t)len);
    str[len] = '\0';
    glfwSetClipboardString(window->m_GLFWwindow, str);
    free(str);
}

void riGui::CreateState(riWindow::HWindow window)
{
    window->m_GuiState = new State();
    nk_init_default(&window->m_GuiState->m_Ctx, 0);
    window->m_GuiState->m_Ctx.clip.userdata = nk_handle_ptr(window);
    window->m_GuiState->m_Ctx.clip.copy = _glfw_clipbard_copy;
    window->m_GuiState->m_Ctx.clip.paste = _glfw_clipbard_paste;

    // Set default font
    if (g_RenderCtx.m_Atlas.default_font)
        nk_style_set_font(&window->m_GuiState->m_Ctx, &g_RenderCtx.m_Atlas.default_font->handle);
}

void riGui::ReleaseState(riWindow::HWindow window)
{
    delete window->m_GuiState;
}

void riGui::BeginFrame(riWindow::HWindow window)
{
    struct GLFWwindow* win = window->m_GLFWwindow;
    HState state = window->m_GuiState;
    nk_context* ctx = &state->m_Ctx;

    glfwGetWindowSize(win, &state->m_WindowWidth, &state->m_WindowHeight);
    glfwGetFramebufferSize(win, &state->m_DisplayWidth, &state->m_DisplayHeight);
    state->m_FBScaleX = (float)state->m_DisplayWidth/(float)state->m_WindowWidth;
    state->m_FBScaleY = (float)state->m_DisplayWidth/(float)state->m_WindowWidth;

    nk_input_begin(ctx);

    for (int i = 0; i < state->m_TextLen; ++i)
        nk_input_unicode(ctx, state->m_Text[i]);

    /* optional grabbing behavior */
    if (ctx->input.mouse.grab)
        glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
    else if (ctx->input.mouse.ungrab)
        glfwSetInputMode(win, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

    nk_input_key(ctx, NK_KEY_DEL, glfwGetKey(win, GLFW_KEY_DELETE) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_ENTER, glfwGetKey(win, GLFW_KEY_ENTER) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_TAB, glfwGetKey(win, GLFW_KEY_TAB) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_BACKSPACE, glfwGetKey(win, GLFW_KEY_BACKSPACE) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_UP, glfwGetKey(win, GLFW_KEY_UP) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_DOWN, glfwGetKey(win, GLFW_KEY_DOWN) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_TEXT_START, glfwGetKey(win, GLFW_KEY_HOME) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_TEXT_END, glfwGetKey(win, GLFW_KEY_END) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SCROLL_START, glfwGetKey(win, GLFW_KEY_HOME) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SCROLL_END, glfwGetKey(win, GLFW_KEY_END) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SCROLL_DOWN, glfwGetKey(win, GLFW_KEY_PAGE_DOWN) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SCROLL_UP, glfwGetKey(win, GLFW_KEY_PAGE_UP) == GLFW_PRESS);
    nk_input_key(ctx, NK_KEY_SHIFT, glfwGetKey(win, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS||
                                    glfwGetKey(win, GLFW_KEY_RIGHT_SHIFT) == GLFW_PRESS);

    if (glfwGetKey(win, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS ||
        glfwGetKey(win, GLFW_KEY_RIGHT_CONTROL) == GLFW_PRESS) {
        nk_input_key(ctx, NK_KEY_COPY, glfwGetKey(win, GLFW_KEY_C) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_PASTE, glfwGetKey(win, GLFW_KEY_V) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_CUT, glfwGetKey(win, GLFW_KEY_X) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_UNDO, glfwGetKey(win, GLFW_KEY_Z) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_REDO, glfwGetKey(win, GLFW_KEY_R) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_WORD_LEFT, glfwGetKey(win, GLFW_KEY_LEFT) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_WORD_RIGHT, glfwGetKey(win, GLFW_KEY_RIGHT) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_LINE_START, glfwGetKey(win, GLFW_KEY_B) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_TEXT_LINE_END, glfwGetKey(win, GLFW_KEY_E) == GLFW_PRESS);
    } else {
        nk_input_key(ctx, NK_KEY_LEFT, glfwGetKey(win, GLFW_KEY_LEFT) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_RIGHT, glfwGetKey(win, GLFW_KEY_RIGHT) == GLFW_PRESS);
        nk_input_key(ctx, NK_KEY_COPY, 0);
        nk_input_key(ctx, NK_KEY_PASTE, 0);
        nk_input_key(ctx, NK_KEY_CUT, 0);
        nk_input_key(ctx, NK_KEY_SHIFT, 0);
    }

    double mouse_x, mouse_y;
    glfwGetCursorPos(win, &mouse_x, &mouse_y);
    nk_input_motion(ctx, (int)mouse_x, (int)mouse_y);

    if (ctx->input.mouse.grabbed) {
        glfwSetCursorPos(win, ctx->input.mouse.prev.x, ctx->input.mouse.prev.y);
        ctx->input.mouse.pos.x = ctx->input.mouse.prev.x;
        ctx->input.mouse.pos.y = ctx->input.mouse.prev.y;
    }

    nk_input_button(ctx, NK_BUTTON_LEFT, (int)mouse_x, (int)mouse_y, glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS);
    nk_input_button(ctx, NK_BUTTON_MIDDLE, (int)mouse_x, (int)mouse_y, glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS);
    nk_input_button(ctx, NK_BUTTON_RIGHT, (int)mouse_x, (int)mouse_y, glfwGetMouseButton(win, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS);
    nk_input_scroll(ctx, state->m_Scroll);
    nk_input_end(ctx);
    state->m_TextLen = 0;
    state->m_Scroll = nk_vec2(0,0);
}

void riGui::EndFrame(riWindow::HWindow window)
{


    nk_clear(&window->m_GuiState->m_Ctx);
}

void riGui::BeginPanel(riWindow::HWindow window, riPanel::HPanelInstance panel_instance, uint64_t x0, uint64_t y0, uint64_t x1, uint64_t y1)
{
    // lua_State* L = riScript::GetLuaState(script);
    // Fetch __window_ptr
    // lua_getglobal(L, "__window_ptr");
    // riWindow::HWindow window = (riWindow::HWindow)lua_touserdata(L, -1);
    // lua_pop(L, 1);

    nk_begin(&window->m_GuiState->m_Ctx, riPanel::GetPanelName(panel_instance), nk_rect(x0, y0, x1-x0, y1-y0), NK_WINDOW_TITLE);

    // nk_layout_row_static(&window->m_GuiState->m_Ctx, 30, 80, 1);
    // if (nk_button_label(&window->m_GuiState->m_Ctx, "button"))
    //     LOG_F(INFO, "button pressed");
}

void riGui::EndPanel(riWindow::HWindow window)
{
    nk_end(&window->m_GuiState->m_Ctx);
    _GuiRender(window->m_GuiState, &window->m_GuiState->m_Ctx, NK_ANTI_ALIASING_OFF, 512 * 1024, 128 * 1024);
}

static inline riWindow::HWindow _GetScriptHWindow(lua_State* L)
{
    // Fetch __window_ptr
    lua_getglobal(L, "__window_ptr");
    riWindow::HWindow window = (riWindow::HWindow)lua_touserdata(L, -1);
    lua_pop(L, 1);
    return window;
}

static inline struct nk_context* _GetScriptNKCtx(lua_State* L)
{
    return &(_GetScriptHWindow(L))->m_GuiState->m_Ctx;
}

static int GuiScript_LayoutRowStatic(lua_State* L)
{
    nk_layout_row_static(_GetScriptNKCtx(L), luaL_checknumber(L, 1), luaL_checkinteger(L, 2), luaL_checkinteger(L, 3));
    return 0;
}

static int GuiScript_LayoutRowDynamic(lua_State* L)
{
    nk_layout_row_dynamic(_GetScriptNKCtx(L), luaL_checknumber(L, 1), luaL_checkinteger(L, 2));
    return 0;
}

static int GuiScript_ButtonLabel(lua_State* L)
{
    lua_pushboolean(L, nk_button_label(_GetScriptNKCtx(L), luaL_checkstring(L, 1)));
    return 1;
}

static nk_color _ParseColor(lua_State* L, int table_idx)
{
    float r = .0f,g = .0f,b = .0f,a = .0f;
    lua_rawgeti(L, table_idx, 1);
    if (lua_isnumber(L, -1)) {
        r = lua_tonumber(L, -1);
    }
    lua_rawgeti(L, table_idx, 2);
    if (lua_isnumber(L, -1)) {
        g = lua_tonumber(L, -1);
    }
    lua_rawgeti(L, table_idx, 3);
    if (lua_isnumber(L, -1)) {
        b = lua_tonumber(L, -1);
    }
    lua_rawgeti(L, table_idx, 4);
    if (lua_isnumber(L, -1)) {
        a = lua_tonumber(L, -1);
    }
    lua_pop(L, 4);

    return nk_rgba_f(r,g,b,a);
}

static int GuiScript_Label(lua_State* L)
{
    int top = lua_gettop(L);

    nk_flags alignment = NK_TEXT_LEFT;
    if (top > 1) {
        alignment = luaL_checkinteger(L, 2);
    }

    if (top > 2) {
        nk_label_colored(_GetScriptNKCtx(L), luaL_checkstring(L, 1), alignment, _ParseColor(L, 3));
    } else {
        nk_label(_GetScriptNKCtx(L), luaL_checkstring(L, 1), alignment);
    }
    return 0;
}

void riGui::RegisterScriptModule(riScript::HScript script)
{
    lua_State* L = riScript::GetLuaState(script);
    int top = lua_gettop(L);
    static luaL_Reg module[] = {
        {"layout_row_static", GuiScript_LayoutRowStatic},
        {"layout_row_dynamic", GuiScript_LayoutRowDynamic},
        {"button_label", GuiScript_ButtonLabel},
        {"label", GuiScript_Label},
        // {"panel_end", GuiScript_PanelEnd},
        {0, 0}
    };

    luaL_register(L, "gui", module);

    // Push enums
#define SETCONSTANT(name, value) \
    lua_pushinteger(L, value); \
    lua_setfield(L, -2, name);

    SETCONSTANT("TEXT_LEFT", NK_TEXT_LEFT);
    SETCONSTANT("TEXT_CENTERED", NK_TEXT_CENTERED);
    SETCONSTANT("TEXT_RIGHT", NK_TEXT_RIGHT);

#undef SETCONSTANT

    lua_pop(L, 1);
    assert(top == lua_gettop(L));
}
