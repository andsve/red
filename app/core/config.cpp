#include "core.h"
#include "config.h"
#include "resource.h"
#include "hash.h"
#include "hashtable.h"

#include <ini.h>
#include <stdlib.h>
#include <string.h>

using namespace riConfig;

enum EntryType
{
    ENTRY_STRING = 0,
    ENTRY_INTEGER = 1,
    ENTRY_FLOAT = 2,
    ENTRY_BOOL = 3
};

struct SEntry
{
    EntryType m_Type;

    union {
        char*    m_String;
        uint32_t m_Uint32;
        int32_t  m_Int32;
        float    m_Float;
    } m_Data;
};

struct SConfig
{
    SConfig() {
        uint32_t tablesize   = uint32_t(1024 / 0.85f);
        uint32_t sizeneeded  = jc::HashTable<uint32_t, SEntry>::CalcSize(tablesize);
        m_Data = malloc(sizeneeded);
        m_HashTable.Create(1024, m_Data);
    }
    ~SConfig() {
        free(m_Data);
    }
    void*                           m_Data;
    jc::HashTable<uint32_t, SEntry> m_HashTable;

} g_Config;

const char* riConfig::GetString(uint32_t id, const char* default_value)
{
    SEntry *entry = g_Config.m_HashTable.Get(id);
    if (entry == 0x0 || entry->m_Type != ENTRY_STRING)
    {
        return default_value;
    }
    return entry->m_Data.m_String;
}

uint32_t riConfig::GetUint32(uint32_t id, uint32_t default_value)
{
    SEntry *entry = g_Config.m_HashTable.Get(id);
    if (entry == 0x0 || entry->m_Type != ENTRY_INTEGER)
    {
        return default_value;
    }
    return entry->m_Data.m_Uint32;
}

int32_t riConfig::GetInt32(uint32_t id, int32_t default_value)
{
    SEntry *entry = g_Config.m_HashTable.Get(id);
    if (entry == 0x0 || entry->m_Type != ENTRY_INTEGER)
    {
        return default_value;
    }
    return entry->m_Data.m_Int32;
}

float riConfig::GetFloat(uint32_t id, float default_value)
{
    SEntry *entry = g_Config.m_HashTable.Get(id);
    if (entry == 0x0 || entry->m_Type != ENTRY_FLOAT)
    {
        return default_value;
    }
    return entry->m_Data.m_Float;
}


const char* riConfig::GetString(const char* id, const char* default_value)
{
    return GetString(riHash(id), default_value);
}

uint32_t riConfig::GetUint32(const char* id, uint32_t default_value)
{
    return GetUint32(riHash(id), default_value);
}

int32_t riConfig::GetInt32(const char* id, int32_t default_value)
{
    return GetInt32(riHash(id), default_value);
}

float riConfig::GetFloat(const char* id, float default_value)
{
    return GetFloat(riHash(id), default_value);
}

static EntryType DetermineType(const char* value)
{
    EntryType guess = ENTRY_INTEGER;
    const char* iter = value;
    while (*iter != '\0')
    {
        char c = *iter;
        if (c < '0' || c > '9') {
            if (iter == value && c == '-') {
                // integer and float can start with -
            } else if (guess == ENTRY_INTEGER && c == '.') {
                guess = ENTRY_FLOAT;
            } else {
                guess = ENTRY_STRING;
                break;
            }
        }
        iter++;
    }

    return guess;
}

static int _ini_handler(void* user, const char* section, const char* name, const char* value)
{
    // Concat section and name
    uint64_t len_section = strlen(section);
    uint64_t len_name = strlen(name);
    uint64_t len_needed = len_section + len_name + 2; // 2 = . + \0 chars
    char* concat_str = (char*)malloc(len_needed * sizeof(char));
    sprintf(concat_str, "%s.%s", section, name);

    SEntry entry;
    entry.m_Type = DetermineType(value);
    if (entry.m_Type == ENTRY_STRING) {
        uint64_t len_value = strlen(value) * sizeof(char) + 1;
        entry.m_Data.m_String = (char*)malloc(len_value);//value;
        memcpy(entry.m_Data.m_String, value, len_value);
        entry.m_Data.m_String[len_value-1] = '\0';
    } else if (entry.m_Type == ENTRY_FLOAT) {
        entry.m_Data.m_Float = atof(value);
    } else {
        entry.m_Data.m_Uint32 = atoll(value);
    }

    g_Config.m_HashTable.Put(riHash(concat_str), entry);

    // Cleanup
    free(concat_str);

    return 1;
}

Result riConfig::Load(const char* path)
{
    char* data = 0x0;
    uint64_t size = 0;
    riRes::Result r = riRes::Get(path, &data, size, true);
    if (r != riRes::RESULT_OK) {
        LOG_F(ERROR, "Could not load configuration file: %s", path);
        return RESULT_ERROR;
    }

    int ini_r = ini_parse_string(data, _ini_handler, 0x0);
    if (ini_r != 0) {
        if (ini_r == -1) {
            LOG_F(ERROR, "Error while parsing configuration file; unable to open.");
            free(data);
            return RESULT_ERROR;
        } else if (ini_r == -2) {
            LOG_F(ERROR, "Error while parsing configuration file; memory allocation error.");
            free(data);
            return RESULT_ERROR;
        } else {
            LOG_F(WARNING, "Syntax error in configuration file line %d.", ini_r);
        }
    }

    free(data);
    return RESULT_OK;
}

void riConfig::Terminate()
{

}
