#include "session.h"
#include "window.h"
#include "script.h"

using namespace riSession;

Result riSession::Load(const char* filepath)
{
    riScript::CreateInstanceParams params;
    params.m_AutoLoadDefaults = true; // FIXME this should be false for session loading...
    params.m_Shared = false;
    riScript::Result r = riScript::CreateInstance(params);
    if (r != riScript::RESULT_OK)
    {
        LOG_F(ERROR, "Could not create session script instance (%u).", r);
        return RESULT_ERROR;
    }
    riScript::HScript script = params.m_Script;

    // Register window script modules
    riWindow::RegisterScriptModule(script);

    r = riScript::LoadFromFile(script, filepath, true);
    if (r != riScript::RESULT_OK)
    {
        LOG_F(ERROR, "Could not load and run session script (%u).", r);
        riScript::DestroyInstance(script);
        return RESULT_ERROR;
    }

    riScript::DestroyInstance(script);

    return RESULT_OK;
}

Result riSession::Save(const char* filepath)
{
    return RESULT_OK;
}
