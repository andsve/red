#ifndef CORE_PANEL_H
#define CORE_PANEL_H

#include "core.h"
#include "script.h"

namespace riWindow
{
    typedef struct Window* HWindow;
}

namespace riPanel
{
    typedef struct Panel* HPanel;
    typedef struct PanelInstance* HPanelInstance;

    enum Result
    {
        RESULT_OK = 0,
        RESULT_PANEL_EXIST,
        RESULT_SCRIPT_ERROR,
        RESULT_ERROR
    };

    struct NewPanelParams
    {
        const char* m_PanelScriptFilepath;
    };

    void InitPanelRegistry();
    Result RegisterPanel(uint32_t id, HPanel panel);
    Result NewPanel(NewPanelParams& params);
    HPanel GetPanel(uint32_t id);

    HPanelInstance NewInstance(HPanel panel, riWindow::HWindow window);
    const char* GetPanelName(HPanelInstance panel_instance);
    void ReleaseInstance(HPanelInstance panel_instance);

    int CreateScriptInstance(HPanel panel);
    void ReleaseScriptInstance(HPanel panel, int script_instance);

    Result Draw(HPanelInstance panel_instance, uint64_t x0, uint64_t y0, uint64_t x1, uint64_t y1);

    void RegisterScriptModule(riScript::HScript script, HPanel panel);
}

#endif // CORE_PANEL_H
