#include "core.h"

#include "window.h"
#include "window_private.h"
#include "script_private.h"

#include <lua/lua.hpp>

using namespace riWindow;

static int WindowScript_Tmp(lua_State* L)
{
    lua_pushstring(L, "hello world");
    return 1;
}

static int GetTableInt(lua_State* L, int index, const char* field, int default_value)
{
    int top = lua_gettop(L);
    int ret = default_value;
    if (lua_type(L, index) == LUA_TTABLE)
    {
        lua_getfield(L, index, field);
        if (lua_type(L, -1) == LUA_TNUMBER)
        {
            ret = lua_tonumber(L, -1);
        }
        lua_pop(L, 1);
    }

    assert(top == lua_gettop(L));
    return ret;
}

// static LayoutNode NewLayoutNode(lua_State* L, int panel_idx)
// {
//     int top = lua_gettop(L);

//     LayoutNode new_node;

//     lua_getfield(L, panel_idx, "size");
//     if (!lua_isnil(L, -1)) {
//         new_node.m_SplitSize = lua_tonumber(L, -1);
//     }
//     lua_pop(L, 1);


//     lua_getfield(L, panel_idx, "panel");
//     if (lua_istable(L, -1)) {

//     } else if (lua_isstring(L, -1)) {

//     }
//     lua_pop(L, 1);

//     assert(top == lua_gettop(L));
//     return new_node;
// }

static int FillLayout(lua_State* L, int layout_idx, riArray<LayoutNode>* nodes, HWindow window)
{
    int top = lua_gettop(L);

    int panel_count = 0;

    lua_pushnil(L);
    while (lua_next(L, layout_idx)) {
        if (lua_type(L, -1) == LUA_TTABLE) {
            panel_count++;

            if (nodes->Remains() == 0) {
                nodes->SetCapacity(nodes->Capacity()*2);
            }

            LayoutNode new_node;
            new_node.m_SplitCount = 0;
            nodes->Put(new_node);

            LayoutNode* node = &nodes->Last();

            lua_getfield(L, -1, "size");
            if (lua_isnumber(L, -1)) {
                node->m_SplitSize = lua_tonumber(L, -1);
            }
            lua_pop(L, 1);

            lua_getfield(L, -1, "vertical");
            if (lua_isnil(L, -1)) {
                node->m_Direction = SPLIT_HORIZONTAL;
            } else {
                node->m_Direction = SPLIT_VERTICAL;
            }
            lua_pop(L, 1);

            lua_getfield(L, -1, "panel");
            if (lua_isstring(L, -1)) {
                uint32_t panel_id_hash = riHash(lua_tostring(L, -1));
                riPanel::HPanel panel = riPanel::GetPanel(panel_id_hash);
                node->m_PanelInstance = riPanel::NewInstance(panel, window);
            } else if (lua_istable(L, -1)) {
                // This is a sub panel node, recurse and fill sub tree
                node->m_SplitCount = FillLayout(L, lua_gettop(L), nodes, window);
            }
            lua_pop(L, 1);

        } else {
            LOG_F(WARNING, "window.create: panels table can only have string fields");
        }
        lua_pop(L, 1);
    }

    assert(top == lua_gettop(L));

    return panel_count;
}

static int WindowScript_Open(lua_State* L)
{
    int top = lua_gettop(L);

    // window.open(window_params, panel_uid, panel_args)
    riWindow::WindowCreationParams win_params;
    win_params.m_Width = GetTableInt(L, 1, "width", 640);
    win_params.m_Height = GetTableInt(L, 1, "height", 480);
    win_params.m_Visible = true;
    win_params.m_Title = "New Window";

    riWindow::Result r = riWindow::CreateWindow(win_params);
    if (r != riWindow::RESULT_OK)
    {
        LOG_F(ERROR, "Failed to create window: %u", r);
    }

    riArray<LayoutNode> layout;
    if (lua_type(L, 1) == LUA_TTABLE)
    {
        // Fill panel tree
        lua_getfield(L, 1, "layout");
        if (lua_type(L, -1) == LUA_TTABLE)
        {
            layout.SetCapacity(8);

            LayoutNode new_node;
            new_node.m_SplitCount = 0;
            layout.Put(new_node);

            LayoutNode* node = &layout.Last();
            node->m_SplitCount = FillLayout(L, lua_gettop(L), &layout, win_params.m_HWindow);
        }
        lua_pop(L, 1);
    }

    riWindow::SetLayout(win_params.m_HWindow, layout);

    assert(top == lua_gettop(L));
    return 0;
}

void riWindow::RegisterScriptModule(riScript::HScript script)
{
    lua_State* L = riScript::GetLuaState(script);
    int top = lua_gettop(L);
    static luaL_Reg module[] = {
        {"tmp", WindowScript_Tmp},
        {"open", WindowScript_Open},
        {0, 0}
    };

    luaL_register(L, "window", module);
    lua_pop(L, 1);
    assert(top == lua_gettop(L));
}

void riWindow::CreateScriptContext(HWindow window, riScript::HScript script)
{
    // Create Lua window instance
    lua_State* L = riScript::GetLuaState(script);
    int top = lua_gettop(L);
    lua_newtable(L);
    window->m_ScriptContext = luaL_ref(L, LUA_GLOBALSINDEX);
    assert(top == lua_gettop(L));
}

static void PushScriptContext(lua_State* L, riWindow::HWindow window)
{
    lua_rawgeti(L, LUA_GLOBALSINDEX, window->m_ScriptContext);
}

void riWindow::ScriptCallOpen(HWindow window, riScript::HScript script)
{
    lua_State* L = riScript::GetLuaState(script);
    int top = lua_gettop(L);
    lua_getfield(L, LUA_GLOBALSINDEX, "open");
    PushScriptContext(L, window);
    lua_call(L, 1, 0);
    assert(top == lua_gettop(L));
}

void riWindow::ScriptCallDraw(HWindow window, riScript::HScript script)
{
    lua_State* L = riScript::GetLuaState(script);
    int top = lua_gettop(L);
    lua_getfield(L, LUA_GLOBALSINDEX, "draw");
    PushScriptContext(L, window);
    lua_call(L, 1, 0);
    assert(top == lua_gettop(L));
}
