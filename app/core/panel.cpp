#include "core.h"
#include "hashtable.h"
#include "graphics.h"
#include "gui.h"
#include "panel.h"
#include "panel_private.h"

using namespace riPanel;

#define MAX_PANEL_INSTANCES 32
static PanelInstance g_PanelInstances[MAX_PANEL_INSTANCES];

static jc::HashTable<uint32_t, HPanel> g_PanelReg;
static void* g_PanelRegData;

void riPanel::InitPanelRegistry()
{
    uint32_t tablesize  = uint32_t(512 / 0.85f);
    uint32_t sizeneeded = jc::HashTable<uint32_t, HPanel>::CalcSize(tablesize);

    g_PanelRegData = malloc(sizeneeded);
    g_PanelReg.Create(512, g_PanelRegData);
}

Result riPanel::RegisterPanel(uint32_t id, HPanel panel)
{
    if (g_PanelReg.Get(id)) {
        return RESULT_PANEL_EXIST;
    }

    LOG_F(INFO, "Registering panel %p with id: 0x%x", panel, id);
    g_PanelReg.Put(id, panel);
    panel->m_Registered = true;

    return RESULT_OK;
}

Result riPanel::NewPanel(NewPanelParams& params)
{
    HPanel panel = new Panel();

    riScript::CreateInstanceParams script_params;
    script_params.m_AutoLoadDefaults = true;
    script_params.m_Shared = false;
    riScript::Result r = riScript::CreateInstance(script_params);
    if (r != riScript::RESULT_OK)
    {
        LOG_F(ERROR, "Could not create panel script instance (%u).", r);
        delete panel;
        return RESULT_SCRIPT_ERROR;
    }
    panel->m_Script = script_params.m_Script;

    // Register panel and graphics script modules
    RegisterScriptModule(panel->m_Script, panel);
    riGraphics::RegisterScriptModule(panel->m_Script);
    riGui::RegisterScriptModule(panel->m_Script);
    riWindow::RegisterScriptModule(panel->m_Script);

    r = riScript::LoadFromFile(panel->m_Script, params.m_PanelScriptFilepath, true);
    if (r != riScript::RESULT_OK)
    {
        LOG_F(ERROR, "Could not load panel script (%u).", r);
        riScript::DestroyInstance(panel->m_Script);
        delete panel;
        return RESULT_SCRIPT_ERROR;
    }

    return RESULT_OK;
}

HPanel riPanel::GetPanel(uint32_t id)
{
    HPanel* p = g_PanelReg.Get(id);
    return p == NULL ? NULL : *p;
}

HPanelInstance riPanel::NewInstance(HPanel panel, riWindow::HWindow window)
{
    for (int i = 0; i < MAX_PANEL_INSTANCES; ++i)
    {
        PanelInstance* panel_instance = &g_PanelInstances[i];
        if (panel_instance->m_Free) {
            panel_instance->m_Free = false;
            panel_instance->m_ScriptInstance = CreateScriptInstance(panel);
            panel_instance->m_Panel = panel;
            panel_instance->m_Window = window;

            ScriptCallInit(panel_instance);

            return panel_instance;
        }
    }

    return NULL;
}

const char* riPanel::GetPanelName(HPanelInstance panel_instance)
{
    return panel_instance->m_Panel->m_Id;
}

void riPanel::ReleaseInstance(HPanelInstance panel_instance)
{
    panel_instance->m_Free = true;
    ReleaseScriptInstance(panel_instance->m_Panel, panel_instance->m_ScriptInstance);
}

Result riPanel::Draw(HPanelInstance panel_instance, uint64_t x0, uint64_t y0, uint64_t x1, uint64_t y1)
{
    ScriptCallDraw(panel_instance, x0, y0, x1, y1);
    return RESULT_OK;
}
