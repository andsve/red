#ifndef RED_CORE_WINDOW_H
#define RED_CORE_WINDOW_H

#include <string.h>

#include "core.h"
#include "array.h"
#include "hash.h"
#include "script.h"
#include "panel.h"

#define MAX_LAYOUT_SPLITS 32
#define INVALID_PANEL_MASK 0x0

namespace riWindow
{
    typedef struct Window* HWindow;

    enum Result
    {
        RESULT_OK = 1,
        RESULT_UNKNOWN_ERROR,
        RESULT_GLFW_ERROR,
        RESULT_GLEW_ERROR,
        RESULT_SCRIPT_ERROR,
        RESULT_ROOT_EXISTS,
        RESULT_OUT_OF_RESOURCES,
        RESULT_NO_ROOT
    };

    enum LayoutSplit
    {
        SPLIT_HORIZONTAL,
        SPLIT_VERTICAL
    };
    struct LayoutNode
    {
        LayoutNode() {
            m_PanelInstance = INVALID_PANEL_MASK;
            m_SplitCount = 0;
            m_SplitSize = 1.0;
            m_Direction = SPLIT_HORIZONTAL;
        }
        riPanel::HPanelInstance m_PanelInstance;
        int m_SplitCount;
        double m_SplitSize;
        LayoutSplit m_Direction;
    };
    struct LayoutRect
    {
        double x0,y0,x1,y1;
    };

    struct Layout
    {
        Layout() {
            for (int i = 0; i < MAX_LAYOUT_SPLITS; ++i)
            {
                m_Tree[i] = LayoutNode();
            }
            m_TreeSize = 0;
        }
        LayoutNode m_Tree[MAX_LAYOUT_SPLITS];
        int m_TreeSize;
    };

    struct WindowCreationParams
    {
        /// In
        uint64_t        m_Width;
        uint64_t        m_Height;
        const char*     m_Title;
        bool            m_Visible;

        /// Out
        HWindow         m_HWindow;
    };

    Result CreateRootWindow();
    Result CreateWindow(WindowCreationParams& params);
    bool SetLayout(HWindow window, riArray<LayoutNode>& layout);
    void PatchNative(HWindow window);
    bool ShouldClose(HWindow window);
    void Close(HWindow window);

    uint32_t GetWindowCount();
    HWindow GetWindow(uint32_t index);

    Result ReloadScript();
    void RegisterScriptModule(riScript::HScript script);
    void CreateScriptContext(HWindow window, riScript::HScript script);
    void ScriptCallOpen(HWindow window, riScript::HScript script);
    void ScriptCallDraw(HWindow window, riScript::HScript script);

    Result Redraw(HWindow window);
    void NeedsRedraw(HWindow window);

}

#endif // RED_CORE_WINDOW_H
