#ifndef RED_CORE_GRAPHICS_H
#define RED_CORE_GRAPHICS_H

#include "script.h"

namespace riGraphics
{
    void RegisterScriptModule(riScript::HScript script);
}

#endif
