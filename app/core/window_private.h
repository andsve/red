#ifndef CORE_WINDOW_PRIVATE_H
#define CORE_WINDOW_PRIVATE_H

#include <GLFW/glfw3.h>

#include "panel.h"
#include "gui.h"

namespace riWindow
{

    struct Window
    {
        Window() {
            memset(this, 0x0, sizeof(Window));
            m_Layout = Layout();
            m_NeedsToDraw = true;
        }

        GLFWwindow*   m_GLFWwindow;

        /// State
        bool          m_NeedsToDraw;

        /// Script
        int           m_ScriptContext;

        /// Layout and panels
        Layout        m_Layout;
        riGui::HState m_GuiState;
    };

}

#endif // CORE_WINDOW_PRIVATE_H
