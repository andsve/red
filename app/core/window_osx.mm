#ifdef __APPLE__
#define GLFW_EXPOSE_NATIVE_NSGL
#define GLFW_EXPOSE_NATIVE_COCOA
#include "GLFW/glfw3.h"
#include "GLFW/glfw3native.h"
#import <objc/runtime.h>
#endif

#include "window.h"
#include "window_private.h"

// @interface TopMainBar : NSView {
// }
// @end

// @implementation TopMainBar
// - (void)drawRect:(NSRect)rect
// {
//     [[NSColor yellowColor] set];
//     NSRectFill(rect);
// }
// -(BOOL)mouseDownCanMoveWindow {
//     return YES;
// }
// @end

void riWindow::PatchNative(HWindow window)
{
    NSWindow* nswindow = glfwGetCocoaWindow(window->m_GLFWwindow);

    nswindow.titlebarAppearsTransparent = true; // gives it "flat" look
    // nswindow.titleVisibility = NSWindowTitleHidden;
    // nswindow.styleMask = NSWindowStyleMaskFullSizeContentView;
    // nswindow.styleMask = NSUnifiedTitleAndToolbarWindowMask;
    // nswindow.appearance = NSAppearanceNameVibrantDark;
    // nswindow.styleMask |= NSWindowStyleMaskFullSizeContentView;
    // nswindow.styleMask &= ~NSWindowStyleMaskTitled;
    // nswindow.styleMask |= NSWindowStyleMaskTexturedBackground;
    // nswindow.styleMask |= NSWindowStyleMaskHUDWindow;
    nswindow.movableByWindowBackground = YES;
    nswindow.titleVisibility = NSWindowTitleHidden;
    // nswindow.appearance = NSAppearanceNameVibrantDark;
    nswindow.backgroundColor = [NSColor colorWithRed:(CGFloat)40.0f/255.0f
                                               green:(CGFloat)40.0f/255.0f
                                                blue:(CGFloat)40.0f/255.0f
                                               alpha:(CGFloat)1.0f];//NSColor.blackColor; // set the background color
    // nswindow.backgroundColor = NSColor.blackColor;
    // nswindow.contentView.mouseDownCanMoveWindow = YES;

    // NSView *drager = [[TopMainBar alloc] initWithFrame:NSMakeRect(100, 100, 100, 100)];
    // [nswindow.contentView addSubview:drager];
}
