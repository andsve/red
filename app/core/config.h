#ifndef CORE_CONFIG_H
#define CORE_CONFIG_H

#include <stdint.h>

namespace riConfig
{
    enum Result
    {
        RESULT_OK = 0,
        RESULT_ERROR
    };

    Result Load(const char* path);
    const char* GetString(uint32_t id, const char* default_value = 0x0);
    uint32_t GetUint32(uint32_t id, uint32_t default_value = 0);
    int32_t GetInt32(uint32_t id, int32_t default_value = 0);
    float GetFloat(uint32_t id, float default_value = 0.0f);

    const char* GetString(const char* id, const char* default_value = 0x0);
    uint32_t GetUint32(const char* id, uint32_t default_value = 0);
    int32_t GetInt32(const char* id, int32_t default_value = 0);
    float GetFloat(const char* id, float default_value = 0.0f);

    void Terminate();
}

#endif // CORE_CONFIG_H
