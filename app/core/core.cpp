#include "core.h"
#include "panel.h"
#include "window.h"
#include "gui.h"
#include "session.h"
#include "array.h"
#include "resource.h"
#include "config.h"
#include "hash.h"

#include <GLFW/glfw3.h>

using namespace riCore;

// struct Application
// {
//     riArray<riWindow::HWindow> m_Windows;
// } g_Application;

static void GLFWErrorCallback(int error, const char* description)
{
    LOG_F(ERROR, "GLFW Error [%u]: %s", error, description);
}

// static Result CreateMainWindow()
// {
//     riWindow::WindowCreationParams wparams;
//     wparams.m_Width   = 400;
//     wparams.m_Height  = 200;
//     wparams.m_Title   = "RedInk";
//     wparams.m_Visible = true;
//     if (riWindow::CreateWindow(wparams) != riWindow::RESULT_OK)
//     {
//         return RESULT_ERROR;
//     }

//     g_Application.m_Windows.Put(wparams.m_HWindow);

//     return RESULT_OK;
// }

Result riCore::Init(int argc, char const *argv[])
{
    // Init logging
    loguru::init(argc, (char**)argv);
    loguru::add_file("redink.log", loguru::Truncate, loguru::Verbosity_MAX);

    // Init resource system
    if (riRes::Init(argv[0]) != riRes::RESULT_OK)
    {
        LOG_F(ERROR, "Could not initialize resource system.");
        return RESULT_ERROR;
    }

    // Load configuration file
    if (riConfig::Load("redink.ini") != riConfig::RESULT_OK)
    {
        LOG_F(WARNING, "Could not load configuration file, application may not behave as expected.");
    }

    // Create main script instance
    riScript::CreateMainInstance();

    // Init panel registry
    riPanel::InitPanelRegistry();

    // Init GLFW
    if (!glfwInit())
    {
        LOG_F(ERROR, "Could not initialize GLFW library.");
        return RESULT_ERROR;
    }
    glfwSetErrorCallback(GLFWErrorCallback);
    DLOG_F(INFO, "GLFW Version: %s", glfwGetVersionString());

    // Create root window
    if (riWindow::CreateRootWindow() != riWindow::RESULT_OK)
    {
        LOG_F(ERROR, "Could not create root window.");
        return RESULT_ERROR;
    }

    // Init GUI system
    riGui::Init();

    // g_Application.m_Windows.SetCapacity(riConfig::GetUint32("windows.capacity", 16));

    // Register built in panels
    {
        // TODO move this into somewhere and automate it somehow
        riPanel::NewPanelParams panel_params;
        panel_params.m_PanelScriptFilepath = "com.pixelbrew.redink/gui/debug_panel.lua";
        riPanel::Result res = riPanel::NewPanel(panel_params);
        if (res != riPanel::RESULT_OK)
        {
            LOG_F(ERROR, "Could not create debug panel: %u", res);
        }
    }
    {
        // TODO move this into somewhere and automate it somehow
        riPanel::NewPanelParams panel_params;
        panel_params.m_PanelScriptFilepath = "com.pixelbrew.redink/gui/about_panel.lua";
        riPanel::Result res = riPanel::NewPanel(panel_params);
        if (res != riPanel::RESULT_OK)
        {
            LOG_F(ERROR, "Could not create about panel: %u", res);
        }
    }
    {
        // TODO move this into somewhere and automate it somehow
        riPanel::NewPanelParams panel_params;
        panel_params.m_PanelScriptFilepath = "com.pixelbrew.redink/gui/canvas_panel.lua";
        riPanel::Result res = riPanel::NewPanel(panel_params);
        if (res != riPanel::RESULT_OK)
        {
            LOG_F(ERROR, "Could not create about panel: %u", res);
        }
    }


    if (riSession::Load("session.lua") != riSession::RESULT_OK)
    {
        LOG_F(ERROR, "Could not load session from disk!");
        return RESULT_ERROR;
    }

    // if (CreateMainWindow() != RESULT_OK)
    // {
    //     LOG_F(ERROR, "Could not create main application window.");
    //     return RESULT_ERROR;
    // }


    return RESULT_OK;
}

void riCore::Run()
{
    while (riWindow::GetWindowCount())
    {
        glfwWaitEvents();

        for (int i = 0; i < riWindow::GetWindowCount();)
        {
            riWindow::HWindow window = riWindow::GetWindow(i);
            riWindow::Redraw(window);

            if (riWindow::ShouldClose(window))
            {
                riWindow::Close(window);
            } else {
                i++;
            }
        }
    }
}

void riCore::Terminate()
{
    riRes::Terminate();
    glfwTerminate();
}
