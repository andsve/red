#include "core.h"
#include "resource.h"
#include "script.h"
#include "script_private.h"

#include <stdlib.h>
#include <string.h>
#include <lua/lua.hpp>

using namespace riScript;

static lua_State* g_MainState = 0x0;

struct riScript::Script
{
    Script() {
        memset(this, 0x0, sizeof(Script));
    }

    lua_State* m_L;
    bool       m_Shared;
    bool       m_AutoLoadDefaults;
};

lua_State* riScript::GetLuaState(HScript script)
{
    assert(script);
    return script->m_L;
}

static void RegisterScriptLibraries(lua_State* L)
{
    luaL_openlibs(L);
}

Result riScript::CreateMainInstance()
{
    g_MainState = luaL_newstate();
    RegisterScriptLibraries(g_MainState);

    return RESULT_OK;
}

Result riScript::CreateInstance(CreateInstanceParams& params)
{
    lua_State* L = 0x0;
    if (params.m_Shared)
    {
        if (!g_MainState)
        {
            LOG_F(ERROR, "Main script state not created.");
            return RESULT_ERROR;
        }
        L = lua_newthread(g_MainState);
        // lua_newtable(L);
        // lua_newtable(L);
        // lua_pushliteral(L, "__index");
        // lua_pushvalue(L, LUA_GLOBALSINDEX);
        // lua_settable(L, -3);
        // lua_setmetatable(L, -2);
        // lua_replace(L, LUA_GLOBALSINDEX);
    } else {
        L = luaL_newstate();
    }

    HScript script = new Script();
    script->m_L = L;

    // DLOG_F(INFO, "script->m_L: %p", script->m_L);
    script->m_AutoLoadDefaults = params.m_AutoLoadDefaults;

    if (params.m_AutoLoadDefaults)
    {
        RegisterScriptLibraries(script->m_L);
    }

    params.m_Script = script;
    return RESULT_OK;
}

void riScript::DestroyInstance(HScript script)
{
    assert(script != 0x0);
    assert(script->m_L != 0x0);
    lua_close(script->m_L);
}

Result riScript::LoadFromFile(HScript script, const char* file_path, bool run)
{
    assert(script != 0x0);
    assert(script->m_L != 0x0);

    char* data = 0x0;
    uint64_t size = 0;
    riRes::Result r = riRes::Get(file_path, &data, size);
    if (r != riRes::RESULT_OK)
    {
        LOG_F(ERROR, "Could not load script: %s", file_path);
        return RESULT_ERROR;
    }

    int top = lua_gettop(script->m_L);

    int lr = luaL_loadbuffer(script->m_L, data, size, file_path);
    if (lr == LUA_ERRSYNTAX)
    {
        LOG_F(ERROR, "Lua syntax error in %s.", file_path);
    }
    else if (lr == LUA_ERRMEM)
    {
        LOG_F(ERROR, "Lua memory allocation error when loading %s.", file_path);
    }
    free(data);

    if (run)
    {
        int pcall_r = lua_pcall(script->m_L, 0, 0, 0);
        if (pcall_r == LUA_ERRMEM)
        {
            LOG_F(ERROR, "Memory allocation error when calling Lua.");
            return RESULT_MEM_ERROR;
        }
        else if (pcall_r == LUA_ERRERR)
        {
            LOG_F(ERROR, "Error running Lua error handling function.");
            return RESULT_ERROR;
        } else if (pcall_r == LUA_ERRRUN) {
            // Regular Lua error
            LOG_F(ERROR, "Lua error:\n%s", lua_tostring(script->m_L, lua_gettop(script->m_L)));
            lua_pop(script->m_L, 1);
            assert(top == lua_gettop(script->m_L));
            return RESULT_SCRIPT_ERROR;
        }
    }

    return RESULT_OK;
}
