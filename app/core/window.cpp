#include "GL/glew.h"
#include <lua/lua.hpp>
extern "C" {
    #include <boxer/boxer.h>
}

#include "window.h"
#include "window_private.h"
#include "panel.h"
#include "script.h"
#include "graphics.h"
#include "gui.h"
#include "gui_private.h"
#include "config.h"

using namespace riWindow;

static HWindow g_RootWindow = nullptr;
static const double g_PanelSplitSize = 10;

struct {
    riArray<HWindow> m_Windows;
} g_State;

struct WindowContext
{
    WindowContext() {
        memset(this, 0x0, sizeof(WindowContext));
    }

    riScript::HScript m_Script;
} g_WindowContext;

static void SetDefaultWindowHints()
{
    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
}

static void _glfw_cb_WindowClose(GLFWwindow *glfw_win)
{
    HWindow window = (HWindow)glfwGetWindowUserPointer(glfw_win);
    riWindow::NeedsRedraw(window);
}

static void _glfw_cb_char(GLFWwindow *glfw_win, unsigned int codepoint)
{
    HWindow window = (HWindow)glfwGetWindowUserPointer(glfw_win);
    riGui::HState state = window->m_GuiState;
    if (state->m_TextLen < 256)
        state->m_Text[state->m_TextLen++] = codepoint;
}

static void _glfw_cb_scroll(GLFWwindow *glfw_win, double xoff, double yoff)
{
    HWindow window = (HWindow)glfwGetWindowUserPointer(glfw_win);
    riGui::HState state = window->m_GuiState;
    state->m_Scroll.x += (float)xoff;
    state->m_Scroll.y += (float)yoff;
}

static void _glfw_cb_mouse_button(GLFWwindow* glfw_win, int button, int action, int mods)
{
    HWindow window = (HWindow)glfwGetWindowUserPointer(glfw_win);
    riGui::HState state = window->m_GuiState;

    double x, y;
    if (button != GLFW_MOUSE_BUTTON_LEFT) return;
    glfwGetCursorPos(glfw_win, &x, &y);
    if (action == GLFW_PRESS)  {
        double dt = glfwGetTime() - state->m_LastButtonClick;
        if (dt > 0.02 && dt < 0.2)
            nk_input_button(&state->m_Ctx, NK_BUTTON_DOUBLE, (int)x, (int)y, nk_true);
        state->m_LastButtonClick = glfwGetTime();
    } else nk_input_button(&state->m_Ctx, NK_BUTTON_DOUBLE, (int)x, (int)y, nk_false);
}

static void SetWindowCallbacks(GLFWwindow* glfw_win)
{
    glfwSetWindowCloseCallback(glfw_win, _glfw_cb_WindowClose);
    glfwSetScrollCallback(glfw_win, _glfw_cb_scroll);
    glfwSetCharCallback(glfw_win, _glfw_cb_char);
    glfwSetMouseButtonCallback(glfw_win, _glfw_cb_mouse_button);
    // TODO more callbacks here
}

Result riWindow::CreateRootWindow()
{
    if (g_RootWindow != nullptr) {
        LOG_F(ERROR, "Root window already exist.");
        return RESULT_ROOT_EXISTS;
    }

    g_RootWindow = new Window();

    SetDefaultWindowHints();
    g_RootWindow->m_GLFWwindow = glfwCreateWindow(32, 32, "RedInk Root Window", 0x0, 0x0);
    DLOG_F(INFO, "Root GLFW Window pointer: %p", g_RootWindow->m_GLFWwindow);

    if (g_RootWindow->m_GLFWwindow == nullptr)
    {
        LOG_F(ERROR, "Could not create GLFW window.");
        return RESULT_GLFW_ERROR;
    }

    Result r = ReloadScript();
    if (r != RESULT_OK)
    {
        glfwDestroyWindow(g_RootWindow->m_GLFWwindow);
        g_RootWindow->m_GLFWwindow = 0x0;
        delete g_RootWindow;
        g_RootWindow = nullptr;
        return RESULT_SCRIPT_ERROR;
    }

    glfwMakeContextCurrent(g_RootWindow->m_GLFWwindow);
    glewExperimental = 1;
    GLenum glew_ret = glewInit();
    if (glew_ret != GLEW_OK)
    {
        LOG_F(ERROR, "Could not init GLEW, OpenGL calls might not work (GLEW error: %s).", glewGetErrorString(glew_ret));
        glfwDestroyWindow(g_RootWindow->m_GLFWwindow);
        g_RootWindow->m_GLFWwindow = 0x0;
        delete g_RootWindow;
        g_RootWindow = nullptr;
        return RESULT_GLEW_ERROR;
    }

    // Setup windows state
    g_State.m_Windows.SetCapacity(riConfig::GetUint32("windows.capacity", 16));

    return RESULT_OK;
}

Result riWindow::CreateWindow(WindowCreationParams& params)
{
    if (g_RootWindow == nullptr || g_RootWindow->m_GLFWwindow == 0x0)
    {
        LOG_F(ERROR, "Cannot create window before root window has been created.");
        return RESULT_NO_ROOT;
    }

    if (g_State.m_Windows.Remains() == 0)
    {
        LOG_F(ERROR, "Cannot create window, window buffer too small. Consider changing 'windows.capacity' config.");
        return RESULT_OUT_OF_RESOURCES;
    }

    HWindow window = new Window();

    SetDefaultWindowHints();
    window->m_GLFWwindow = glfwCreateWindow(params.m_Width, params.m_Height, params.m_Title, 0x0, g_RootWindow->m_GLFWwindow);
    glfwSetWindowUserPointer(window->m_GLFWwindow, (void*)window);
    SetWindowCallbacks(window->m_GLFWwindow);

    if (params.m_Visible) {
        glfwShowWindow(window->m_GLFWwindow);
    }

    // Patch window with any needed native calls for the platform.
    PatchNative(window);

    // Create Gui State
    riGui::CreateState(window);

    CreateScriptContext(window, g_WindowContext.m_Script);
    ScriptCallOpen(window, g_WindowContext.m_Script);

    params.m_HWindow = window;
    g_State.m_Windows.Put(window);

    return RESULT_OK;
}

bool riWindow::SetLayout(HWindow window, riArray<LayoutNode>& layout)
{
    // If the window has predefined panels, add them to the tree
    size_t panels_c = layout.Size();
    if (panels_c > 0) {
        window->m_Layout.m_TreeSize = panels_c;
        LOG_F(INFO, "layout tree size: %lu", panels_c);
        for (int i = 0; i < panels_c; ++i) {
            window->m_Layout.m_Tree[i] = layout[i];
            LOG_F(INFO, "panel: %u (container: %s [%u] dir: %s)", i, layout[i].m_SplitCount == 0 ? "no" : "yes", layout[i].m_SplitCount, layout[i].m_Direction == SPLIT_VERTICAL ? "vertical" : "horizontal");
        }
    }

    return true;
}

bool riWindow::ShouldClose(HWindow window)
{
    bool should_close = glfwWindowShouldClose(window->m_GLFWwindow);
    if (should_close && GetWindowCount() == 1)
    {
        if (BoxerSelectionNo == boxerShow("You have unsaved changes, are you sure?", "Quit", BoxerStyleQuestion, BoxerButtonsYesNo))
        {
            should_close = false;
            glfwSetWindowShouldClose(window->m_GLFWwindow, false);
        }
    }
    return should_close;
}

void riWindow::Close(HWindow window)
{
    glfwDestroyWindow(window->m_GLFWwindow);
    g_State.m_Windows.Remove(window);
    delete window;
}

uint32_t riWindow::GetWindowCount()
{
    return g_State.m_Windows.Size();
}

HWindow riWindow::GetWindow(uint32_t index)
{
    assert(index < g_State.m_Windows.Size());
    return g_State.m_Windows[index];
}

void riWindow::NeedsRedraw(HWindow window)
{
    window->m_NeedsToDraw = true;
}

static int DrawPanel(riWindow::HWindow window, LayoutNode* nodes, int idx, LayoutRect draw_rect)
{
    // LOG_F(INFO, "%*s %d", depth, "", idx);
    LayoutNode* node = &nodes[idx];
    idx+=1;
    // printf("rendering %d - %p (inst: %p)\n", idx, node, node->m_PanelInstance);
    if (node->m_SplitCount != 0)
    {
        LayoutSplit direction = node->m_Direction;
        // LOG_F(INFO, "container is %s has %u splits", direction == SPLIT_VERTICAL ? "vertical" : "horizontal", node->m_SplitCount);
        double x = draw_rect.x0;
        double y = draw_rect.y0;
        double w = draw_rect.x1 - draw_rect.x0;
        double h = draw_rect.y1 - draw_rect.y0;
        double last_end = 0.0;
        if (direction == SPLIT_HORIZONTAL) {
            w -= g_PanelSplitSize*(node->m_SplitCount-1);
            last_end = x;
        } else {
            h -= g_PanelSplitSize*(node->m_SplitCount-1);
            last_end = y;
        }

        for (int i = 0; i < node->m_SplitCount; ++i)
        {
            LayoutNode* sub_node = &nodes[idx];

            // Calculate sub draw rect
            LayoutRect sub_rect;
            if (direction == SPLIT_HORIZONTAL) {
                sub_rect.x0 = last_end;
                sub_rect.x1 = x + w * sub_node->m_SplitSize;
                sub_rect.y0 = y;
                sub_rect.y1 = y+h;
                last_end = sub_rect.x1 + g_PanelSplitSize;
            } else {
                sub_rect.y0 = last_end;
                sub_rect.y1 = y + h * sub_node->m_SplitSize;
                sub_rect.x0 = x;
                sub_rect.x1 = x+w;
                last_end = sub_rect.y1 + g_PanelSplitSize;
            }

            if (i == node->m_SplitCount-1) {
                // If last node in split, ignore size and fill rest
                sub_rect.x1 = draw_rect.x1;
                sub_rect.y1 = draw_rect.y1;
            }

            idx = DrawPanel(window, nodes, idx, sub_rect);
        }

    } else {
        // This is a node
        glScissor(draw_rect.x0, draw_rect.y0, draw_rect.x1 - draw_rect.x0, draw_rect.y1 - draw_rect.y0);
        riGui::BeginPanel(window, node->m_PanelInstance, (uint64_t)draw_rect.x0, (uint64_t)draw_rect.y0, (uint64_t)draw_rect.x1, (uint64_t)draw_rect.y1);
        riPanel::Draw(node->m_PanelInstance, (uint64_t)draw_rect.x0, (uint64_t)draw_rect.y0, (uint64_t)draw_rect.x1, (uint64_t)draw_rect.y1);
        riGui::EndPanel(window);
    }

    return idx;
}

Result riWindow::Redraw(HWindow window)
{

    GLFWwindow* glfw_win = window->m_GLFWwindow;
    if (window->m_NeedsToDraw) {
        // window->m_NeedsToDraw = false;

        glfwMakeContextCurrent(glfw_win);

        int fb_width = 0;
        int fb_height = 0;
        glfwGetWindowSize(glfw_win, &fb_width, &fb_height);
        // glfwGetFramebufferSize(glfw_win, &fb_width, &fb_height);
        LayoutRect window_rect = {0.0,0.0,(double)fb_width,(double)fb_height};

        // Setup GL state
        glEnable(GL_SCISSOR_TEST);
        glScissor(window_rect.x0, window_rect.y0, window_rect.x1, window_rect.y1);

        // Call the window draw() function
        riGui::BeginFrame(window);
        ScriptCallDraw(window, g_WindowContext.m_Script);

        // Call the draw() functions for all panels
        DrawPanel(window, window->m_Layout.m_Tree, 0, window_rect);
        riGui::EndFrame(window);

        // Reset GL state
        glDisable(GL_SCISSOR_TEST);

        glfwSwapBuffers(glfw_win);
    }
    return RESULT_OK;
}

Result riWindow::ReloadScript()
{
    if (g_WindowContext.m_Script != 0x0)
    {
        riScript::DestroyInstance(g_WindowContext.m_Script);
        g_WindowContext.m_Script = 0x0;
    }

    riScript::CreateInstanceParams params;
    params.m_AutoLoadDefaults = true;
    params.m_Shared = false;
    riScript::Result r = riScript::CreateInstance(params);
    if (r != riScript::RESULT_OK)
    {
        LOG_F(ERROR, "Could not create window script instance (%u).", r);
        return RESULT_SCRIPT_ERROR;
    }
    g_WindowContext.m_Script = params.m_Script;

    // Register window script modules
    RegisterScriptModule(g_WindowContext.m_Script);
    riGraphics::RegisterScriptModule(g_WindowContext.m_Script);

    r = riScript::LoadFromFile(g_WindowContext.m_Script, "com.pixelbrew.redink/gui/window.lua", true);
    if (r != riScript::RESULT_OK)
    {
        LOG_F(ERROR, "Could not load window script (%u).", r);
        riScript::DestroyInstance(g_WindowContext.m_Script);
        g_WindowContext.m_Script = 0x0;

        return RESULT_SCRIPT_ERROR;
    }

    return RESULT_OK;
}
