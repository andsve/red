#ifndef CORE_SCRIPT_PRIVATE_H
#define CORE_SCRIPT_PRIVATE_H

#include "script.h"
#include <lua/lua.hpp>

namespace riScript
{
    lua_State* GetLuaState(HScript script);
}

#endif // CORE_SCRIPT_PRIVATE_H
