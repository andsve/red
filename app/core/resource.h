#ifndef CORE_RESOURCE_H
#define CORE_RESOURCE_H

#include "core.h"

namespace riRes
{

    // typedef struct Resource* HResource;

    enum Result
    {
        RESULT_OK = 0,
        RESULT_ERROR
    };

    Result Init(const char* argv0);
    void Terminate();

    Result Get(const char* path, char** data, uint64_t& size, bool auto_terminate = false);
    // Result GetRef(const char* path, char** data, uint64_t& size, bool auto_terminate = false);
}

#endif // CORE_RESOURCE_H
