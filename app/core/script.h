#ifndef CORE_SCRIPT_H
#define CORE_SCRIPT_H

namespace riScript
{
    typedef struct Script* HScript;

    enum Result
    {
        RESULT_OK = 0,
        RESULT_ERROR,
        RESULT_SCRIPT_ERROR,
        RESULT_MEM_ERROR
    };

    struct CreateInstanceParams
    {
        /// IN
        bool     m_AutoLoadDefaults;
        bool     m_Shared;
        /// OUT
        HScript  m_Script;
    };

    Result CreateMainInstance();
    Result CreateInstance(CreateInstanceParams& params);
    void DestroyInstance(HScript script);
    Result LoadFromFile(HScript script, const char* file_path, bool run);
}

#endif // CORE_SCRIPT_H
