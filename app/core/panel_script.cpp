#include "core.h"

#include "hash.h"
#include "panel.h"
#include "panel_private.h"
#include "script_private.h"
#include <lua/lua.hpp>

#define INTERNAL_SCRIPT_PANEL_FIELD "__panel_pointer"

using namespace riPanel;

static HPanel GetPanelFromScript(lua_State* L);

static int PanelScript_Register(lua_State* L)
{
    // const char* id_str = luaL_checkstring(L, 1);
    size_t id_len;
    const char* id_str = luaL_checklstring(L, 1, &id_len);
    hash_t id = riHash(id_str);

    HPanel panel = GetPanelFromScript(L);
    if (panel == NULL) {
        luaL_error(L, "Invalid call to panel.register, could not get internal panel pointer.");
    }

    if (panel->m_Registered) {
        luaL_error(L, "Panel already registered.");
    }

    Result res = RegisterPanel(id, panel);
    if (res == riPanel::RESULT_PANEL_EXIST) {
        luaL_error(L, "There is already a panel registered with that id: %s", id_str);
    }

    panel->m_Id = (char*)malloc(id_len+1);
    memcpy(panel->m_Id, id_str, id_len);
    panel->m_Id[id_len] = '\0';

    return 0;
}

static HPanel GetPanelFromScript(lua_State* L)
{
    lua_getglobal(L, INTERNAL_SCRIPT_PANEL_FIELD);
    if (lua_type(L, -1) != LUA_TLIGHTUSERDATA)
    {
        lua_pop(L, 1);
        return NULL;
    }
    HPanel panel = (HPanel)lua_touserdata(L, -1);
    lua_pop(L, 1);
    return panel;
}

HPanel riPanel::GetPanelFromScript(riScript::HScript script)
{
    assert(script != NULL);
    lua_State* L = riScript::GetLuaState(script);
    assert(L != NULL);
    return GetPanelFromScript(L);
}

void riPanel::RegisterScriptModule(riScript::HScript script, HPanel panel)
{
    lua_State* L = riScript::GetLuaState(script);
    int top = lua_gettop(L);
    static luaL_Reg module[] = {
        {"register", PanelScript_Register},
        {0, 0}
    };

    luaL_register(L, "panel", module);
    lua_pop(L, 1);

    // Store panel pointer
    lua_pushlightuserdata(L, panel);
    lua_setglobal(L, INTERNAL_SCRIPT_PANEL_FIELD);

    assert(top == lua_gettop(L));
}

int riPanel::CreateScriptInstance(HPanel panel)
{
    lua_State* L = riScript::GetLuaState(panel->m_Script);
    int top = lua_gettop(L);
    lua_newtable(L);
    int r = luaL_ref(L, LUA_GLOBALSINDEX);
    assert(top == lua_gettop(L));
    return r;
}

static void PushScriptInstance(lua_State* L, HPanelInstance panel_instance)
{
    lua_rawgeti(L, LUA_GLOBALSINDEX, panel_instance->m_ScriptInstance);
}

void riPanel::ReleaseScriptInstance(HPanel panel, int script_instance)
{
    lua_State* L = riScript::GetLuaState(panel->m_Script);
    luaL_unref(L, LUA_GLOBALSINDEX, script_instance);
}

void riPanel::ScriptCallInit(HPanelInstance panel_instance)
{
    HPanel panel = panel_instance->m_Panel;
    lua_State* L = riScript::GetLuaState(panel->m_Script);
    int top = lua_gettop(L);
    lua_getfield(L, LUA_GLOBALSINDEX, "init");
    PushScriptInstance(L, panel_instance);
    lua_call(L, 1, 0);
    assert(top == lua_gettop(L));
}

void riPanel::ScriptCallDraw(HPanelInstance panel_instance, uint64_t x0, uint64_t y0, uint64_t x1, uint64_t y1)
{
    HPanel panel = panel_instance->m_Panel;
    lua_State* L = riScript::GetLuaState(panel->m_Script);
    int top = lua_gettop(L);

    // Push window pointer, as needed by some gui.* functions
    lua_pushlightuserdata(L, panel_instance->m_Window);
    lua_setglobal(L, "__window_ptr");

    lua_getfield(L, LUA_GLOBALSINDEX, "draw");
    PushScriptInstance(L, panel_instance);
    lua_pushnumber(L, x0);
    lua_pushnumber(L, y0);
    lua_pushnumber(L, x1);
    lua_pushnumber(L, y1);
    lua_call(L, 5, 0);
    assert(top == lua_gettop(L));
}
