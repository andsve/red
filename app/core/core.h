#ifndef RED_CORE_H
#define RED_CORE_H

#include <stdint.h>

#include "loguru.h"

namespace riCore
{

    enum Result
    {
        RESULT_OK = 1,
        RESULT_ERROR
    };

    Result Init(int argc, char const *argv[]);
    void Run();
    void Terminate();

}

#endif // RED_CORE_H
