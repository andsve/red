#ifndef CORE_SESSION_H
#define CORE_SESSION_H

namespace riSession
{

    enum Result
    {
        RESULT_OK = 0,
        RESULT_ERROR
    };

    Result Load(const char* filepath);
    Result Save(const char* filepath);
}

#endif // CORE_SESSION_H
