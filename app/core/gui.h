#ifndef RED_CORE_GUI_H
#define RED_CORE_GUI_H

#include "script.h"
#include "window.h"
#include "panel.h"

namespace riGui
{
    typedef struct State* HState;

    void Init();

    void CreateState(riWindow::HWindow window);
    void ReleaseState(riWindow::HWindow window);

    void BeginFrame(riWindow::HWindow window);
    void BeginPanel(riWindow::HWindow window, riPanel::HPanelInstance panel_instance, uint64_t x0, uint64_t y0, uint64_t x1, uint64_t y1);
    void EndPanel(riWindow::HWindow window);
    void EndFrame(riWindow::HWindow window);
    void RegisterScriptModule(riScript::HScript script);
}

#endif // RED_CORE_GUI_H
