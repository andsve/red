#ifndef CORE_HASH_H
#define CORE_HASH_H

#include <stdint.h>

typedef uint32_t hash_t;
hash_t riHash(const void* data, uint64_t len);
hash_t riHash(const char* data_z_term);

#endif // CORE_HASH_H
