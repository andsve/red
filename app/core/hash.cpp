#include "hash.h"
#include "murmurhash.h"

#include <string.h>

#define RI_HASH_SEED 0

hash_t riHash(const void* data, uint64_t len)
{
    hash_t out = 0;
    MurmurHash3_x64_128(data, len, RI_HASH_SEED, &out);
    return out;
}

hash_t riHash(const char* data_z_term)
{
    uint64_t len = strlen(data_z_term);
    return riHash((const void*)data_z_term, len);
}
