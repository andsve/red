#ifndef RED_CORE_PANEL_PRIVATE_H
#define RED_CORE_PANEL_PRIVATE_H

#include <string.h>

#include "panel.h"
#include "script.h"
#include "window.h"

namespace riPanel
{
    struct Panel
    {
        Panel()
        {
            memset(this, 0x0, sizeof(Panel));
        }

        riScript::HScript m_Script;
        char*             m_Id;
        bool              m_Registered;
    };

    struct PanelInstance
    {
        PanelInstance()
        {
            memset(this, 0x0, sizeof(PanelInstance));
            m_Free = true;
        }

        HPanel m_Panel;
        riWindow::HWindow m_Window;
        int    m_ScriptInstance;
        bool   m_Free : 1;
    };

    HPanel GetPanelFromScript(riScript::HScript script);
    void ScriptCallInit(HPanelInstance panel_instance);
    void ScriptCallDraw(HPanelInstance panel_instance, uint64_t x0, uint64_t y0, uint64_t x1, uint64_t y1);
}

#endif // RED_CORE_PANEL_PRIVATE_H
