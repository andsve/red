#include "core.h"
#include "graphics.h"
#include "script_private.h"

#include <GLFW/glfw3.h>
// #include <gl3.h>

using namespace riGraphics;

static int GraphicsScript_ClearColor(lua_State* L)
{
    glClearColor(luaL_checknumber(L, 1), luaL_checknumber(L, 2), luaL_checknumber(L, 3), luaL_checknumber(L, 4));
    return 0;
}

static int GraphicsScript_Clear(lua_State* L)
{
    glClear(GL_COLOR_BUFFER_BIT);
    return 0;
}

void riGraphics::RegisterScriptModule(riScript::HScript script)
{
    lua_State* L = riScript::GetLuaState(script);
    int top = lua_gettop(L);
    static luaL_Reg module[] = {
        {"clear_color", GraphicsScript_ClearColor},
        {"clear", GraphicsScript_Clear},
        {0, 0}
    };

    luaL_register(L, "gfx", module);
    lua_pop(L, 1);
    assert(top == lua_gettop(L));
}
