#include "core/core.h"

int main(int argc, char const *argv[])
{
    if (riCore::Init(argc, argv) == riCore::RESULT_OK) {
        riCore::Run();
    }
    riCore::Terminate();
    return 0;
}
