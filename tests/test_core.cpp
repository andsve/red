#include <gtest/gtest.h>

#include <core/array.h>
#include <core/config.h>
#include <core/hash.h>
#include <core/core.h>
#include <core/resource.h>

/*
class CoreTest : public ::testing::Test
{
protected:
    virtual void SetUp()
    {
    }

    virtual void TearDown()
    {
    }
};
*/

// TEST_F(CoreTest, WindowCreation) // <-

TEST(ArrayTests, Creation)
{
    riArray<int> a;
    ASSERT_EQ(0, a.Capacity());
    ASSERT_EQ(0, a.Size());

    riArray<int> b(0,0);
    ASSERT_EQ(0, b.Capacity());
    ASSERT_EQ(0, b.Size());

    riArray<int> c(1,0);
    ASSERT_EQ(1, c.Capacity());
    ASSERT_EQ(0, c.Size());

    riArray<int> d(1,1);
    ASSERT_EQ(1, d.Capacity());
    ASSERT_EQ(1, d.Size());
}

TEST(ArrayTests, SettersGetters)
{
    riArray<int> a;

    a.SetCapacity(1);
    ASSERT_EQ(1, a.Capacity());
    ASSERT_EQ(0, a.Size());

    a.SetSize(1);
    ASSERT_EQ(1, a.Size());
}

TEST(ArrayTests, Assign)
{
    riArray<int> a;
    a.SetCapacity(1);
    a.SetSize(1);

    a[0] = 9999;
    ASSERT_EQ(9999, a[0]);

    a[0] = 1111;
    ASSERT_EQ(1111, a[0]);

    a.SetCapacity(2);
    ASSERT_EQ(1, a.Size());
    ASSERT_EQ(1111, a[0]);

    a[1] = 9999;
    ASSERT_EQ(9999, a[1]);
}

TEST(ArrayTests, Put)
{
    riArray<int> a(2,0);
    a.Put(9999);
    ASSERT_EQ(9999, a[0]);

    a[0] = 1111;
    ASSERT_EQ(1111, a[0]);

    a.Put(9999);
    ASSERT_EQ(9999, a[1]);
}

TEST(ArrayTests, OutOfBounds)
{
    riArray<int> a(0,0);

    ASSERT_DEATH({ a.Put(9999); }, "Assertion failed:.*");

    a.SetCapacity(2);
    a.Put(9999);
    a.Put(9999);
    ASSERT_DEATH({ a.Put(9999); }, "Assertion failed:.*");
}

TEST(ArrayTests, RemoveAll)
{
    riArray<int> a(4,0);
    ASSERT_EQ(0, a.Size());
    a.Put(1);
    a.Put(2);
    a.Put(3);
    a.RemoveAll(1);
    ASSERT_EQ(3, a.Size());

}

TEST(ConfigTests, TypeChecks)
{
    riConfig::Result r = riConfig::Load("tests/test.ini");
    ASSERT_EQ(riConfig::RESULT_OK, r);

    // [test]
    // s_0 = cepa
    // ui32_0 = 1
    // ui32_1 = -1
    // i32_0 = 1
    // i32_1 = -1
    // f_0 = 123.4
    // f_1 = -123.4
    // f_2 = .123

    /// Valid gets

    // Strings
    ASSERT_STREQ("cepa", riConfig::GetString("test.s_0"));

    // Signed and unsigned 32bit ints
    ASSERT_EQ(1, riConfig::GetUint32("test.ui32_0"));
    ASSERT_EQ(1, riConfig::GetInt32("test.ui32_0"));
    ASSERT_EQ(uint32_t(-1), riConfig::GetUint32("test.ui32_1"));
    ASSERT_EQ(-1, riConfig::GetInt32("test.ui32_1"));
    ASSERT_EQ(1, riConfig::GetUint32("test.i32_0"));
    ASSERT_EQ(1, riConfig::GetInt32("test.i32_0"));
    ASSERT_EQ(uint32_t(-1), riConfig::GetUint32("test.i32_1"));
    ASSERT_EQ(-1, riConfig::GetInt32("test.i32_1"));

    // Floats
    ASSERT_EQ(123.4f, riConfig::GetFloat("test.f_0"));
    ASSERT_EQ(-123.4f, riConfig::GetFloat("test.f_1"));
    ASSERT_EQ(0.123f, riConfig::GetFloat("test.f_2"));

    /// Invalid gets

    ASSERT_STREQ(0, riConfig::GetString("unknown.unknown"));
    ASSERT_EQ(0, riConfig::GetUint32("unknown.unknown"));
    ASSERT_EQ(0, riConfig::GetInt32("unknown.unknown"));
    ASSERT_EQ(0, riConfig::GetInt32("test.f_0"));
    ASSERT_EQ(0, riConfig::GetUint32("test.f_0"));
    ASSERT_EQ(0, riConfig::GetInt32("test.f_1"));
    ASSERT_EQ(0, riConfig::GetUint32("test.f_1"));
}

TEST(CoreTests, LifeCycle)
{
    const int argc = 1;
    const char* argv[] = {"dummy", 0x0};
    ASSERT_EQ(riCore::RESULT_OK, riCore::Init(argc, argv));
    riCore::Terminate();
}

int main(int argc, char **argv)
{
    testing::InitGoogleTest(&argc, argv);
    riRes::Init(argv[0]);
    int ret = RUN_ALL_TESTS();
    riRes::Terminate();
    return ret;
}
