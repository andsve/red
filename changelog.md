TODO
- Only draw panels that need to redraw.
- Extend gfx.* Lua namespace with more functions.
- Create gui.* Lua namespace by exposing nuklear.
- Better error handling for panel instnace/window creation.
- Better Lua error handling.


Changelog

2017
- July 16 - Started on using and exposing nuklear. Panels now render nuklear frames.
- July 14 - Panels are now drawn correctly, with panel spacing etc.
- July 11 - Windows now create a list of panel instances. Panel instances know their owning panel and consist of a script instance. The draw is called for each panel in a window with their panel script instance.
- July 10 - Window creation can take a list of panels. Panels can register themselves.
- June 8 - Started on panels.
