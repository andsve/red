solution "redink"
   configurations { "Debug", "Release" }
   location "build"
   targetdir "bin"
   debugdir "./runtime/"
   platforms { "x64" }


   -- App project
   -- ============
   project "redink"
      language "C++"
      files { "app/*.cpp", "app/*.c" }

      includedirs { "deps/macos/includes",
                    "libs/Boxer/include/",
                    "libs/glfw/include/",
                    "libs/glew/",
                    "libs/nuklear/"
                  }

      links { "glfw", "glew", "boxer", "ri_core", "inih" }

      if os.get() == "windows" then
        links { "opengl32",
                "imm32",
               }

      elseif (os.get() == "macosx") then
        links { "Cocoa.framework" }
        links { "IOKit.framework" }
        links { "CoreFoundation.framework" }
        links { "CoreVideo.framework" }
        links { "OpenGL.framework" }

        links { "deps/macos/libs/libluajit-5.1.a",
                "deps/macos/libs/libphysfs.a" }

        buildoptions {
        -- buildoptions { "-Wall",
                       -- "-Werror",
                       "-std=c++11"
                     }
        linkoptions { "-pagezero_size 10000",
                      "-image_base 100000000"
                    }
      end

      configuration "Debug"
         kind "ConsoleApp"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         kind "WindowedApp"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }

   -- Tests
   project "test"
      kind "ConsoleApp"
      language "C++"
      files { "tests/**.cpp" }

      includedirs { "deps/macos/includes",
                    "libs/glfw/include/",
                    "libs/glew/",
                    "libs/nuklear/",
                    "app/"
                  }
      links { "glfw", "glew", "boxer", "ri_core", "inih" }

      if os.get() == "windows" then
         defines { "WIN32", "WIN32_LEAN_AND_MEAN", "VC_EXTRALEAN" }
      end

      if os.get() == "windows" then
        links { "opengl32",
                "imm32",
               }

      elseif (os.get() == "macosx") then
        links { "Cocoa.framework" }
        links { "IOKit.framework" }
        links { "CoreFoundation.framework" }
        links { "CoreVideo.framework" }
        links { "OpenGL.framework" }

        links { "deps/macos/libs/libgtest.a",
                "deps/macos/libs/libluajit-5.1.a",
                "deps/macos/libs/libphysfs.a" }

        buildoptions { "-Wall", "-Werror", "-std=c++11" }
      end

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }

   -- RedInk core lib
   -- ============
   project "ri_core"
      targetname ("ri_core_internal")
      kind "StaticLib"
      language "C++"
      files { "app/core/**.cpp", "app/core/**.mm" }

      includedirs { "deps/macos/includes",
                    "libs/inih/",
                    "libs/Boxer/include/",
                    "libs/glfw/include/",
                    "libs/glew/",
                    "libs/nuklear/"
                  }

      links { "glfw", "glew", "boxer" }

      if os.get() == "windows" then
        links { "opengl32",
                "imm32",
               }

      elseif (os.get() == "macosx") then
        links { "Cocoa.framework" }
        links { "IOKit.framework" }
        links { "CoreFoundation.framework" }
        links { "CoreVideo.framework" }
        links { "OpenGL.framework" }

        -- buildoptions { "-Wall", "-Werror", "-std=c++11" }
        buildoptions { "-Wall", "-std=c++11" }
      end

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }

   -- GLFW 3.3.0
   project "glfw"
      targetname ("glfw_internal")
      kind "StaticLib"
      language "C"
      files { "libs/glfw/src/context.c",
              "libs/glfw/src/init.c",
              "libs/glfw/src/input.c",
              "libs/glfw/src/monitor.c",
              "libs/glfw/src/vulkan.c",
              "libs/glfw/src/window.c",
      }

      defines { "_GLFW_USE_OPENGL" }
      defines { "_GLFW_USE_CHDIR" }

      if os.get() == "windows" then
          -- defines { "_GLFW_USE_DWM_SWAP_INTERVAL" } -- "Set swap interval even when DWM compositing is enabled"
          -- defines { "_GLFW_USE_OPTIMUS_HPG" } -- "Force use of high-performance GPU on Optimus systems"

         defines { "_WIN32" }
         defines { "_GLFW_WGL" }
         defines { "_GLFW_WIN32" }
         files { "libs/GLFW/src/win32_init.c",
                 "libs/GLFW/src/win32_monitor.c",
                 "libs/GLFW/src/win32_time.c",
                 "libs/GLFW/src/win32_tls.c",
                 "libs/GLFW/src/win32_window.c",
                 "libs/GLFW/src/wgl_context.c",
                 "libs/GLFW/src/winmm_joystick.c" }
         buildoptions { "/wd4996" }

         links { "opengl32" }
      elseif (os.get() == "macosx") then
        defines { "_GLFW_USE_RETINA" }
        defines { "_GLFW_COCOA" }
        defines { "_GLFW_NSGL" }
        files { "libs/GLFW/src/cocoa_init.m",
                "libs/GLFW/src/cocoa_joystick.m",
                "libs/GLFW/src/cocoa_monitor.m",
                "libs/GLFW/src/cocoa_time.c",
                "libs/GLFW/src/cocoa_window.m",
                "libs/GLFW/src/posix_tls.c",
                "libs/GLFW/src/nsgl_context.m",
                "libs/GLFW/src/egl_context.c",
                "libs/GLFW/src/osmesa_context.c",
              }
        buildoptions { "-Wall", "-Werror" }
        buildoptions { "-fno-common" }
      end

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }

   -- GLEW 2.0
   project "glew"
      targetname ("glew_internal")
      kind "StaticLib"
      language "C"
      files { "libs/glew/glew.c" }

      includedirs { "libs/glew/" }

      defines { "GLEW_STATIC", "_LIB" }
      if os.get() == "windows" then
         defines { "WIN32", "WIN32_LEAN_AND_MEAN", "VC_EXTRALEAN" }
         -- links { "opengl32" }
      end

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }

   -- Boxer
   project "boxer"
      targetname ("boxer_internal")
      kind "StaticLib"
      language "C"
      -- files { "libs/Boxer/inlcude/boxer/boxer.h" }

      includedirs { "libs/Boxer/include/" }

      if os.get() == "windows" then
        defines { "WIN32", "WIN32_LEAN_AND_MEAN", "VC_EXTRALEAN" }
        files { "libs/Boxer/src/boxer_win.c" }
        -- links { "opengl32" }
      elseif os.get() == "macosx" then
        files { "libs/Boxer/src/boxer_osx.m" }
      else
        files { "libs/Boxer/src/boxer_linux.c" }
      end

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }

   -- inih - ini loader
   project "inih"
      targetname ("inih_internal")
      kind "StaticLib"
      language "C"
      files { "libs/inih/ini.c" }

      configuration "Debug"
         defines { "DEBUG" }
         flags { "Symbols", "StaticRuntime" }

      configuration "Release"
         defines { "NDEBUG" }
         flags { "Optimize", "StaticRuntime" }
